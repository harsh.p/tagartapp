USE [TagArtDB]
GO
/****** Object:  UserDefinedFunction [dbo].[Distance]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Distance]( @lat1 float , @long1 float , @lat2 float , @long2 float)     
RETURNS float     
     
AS  
BEGIN
     
   DECLARE @DegToRad float = 57.29577951
   DECLARE @Ans float = 0
   DECLARE @Miles float = 0  
     
   SET @Ans = SIN(@lat1 / @DegToRad) * SIN(@lat2 / @DegToRad) + COS(@lat1 / @DegToRad ) * COS( @lat2 / @DegToRad ) * COS(ABS(@long2 - @long1 )/@DegToRad)     
     
   SET @Miles = 3959 * ATAN(SQRT(1 - SQUARE(@Ans)) / @Ans)     
     
   SET @Miles = ROUND(@Miles,1)   
     
   RETURN (@Miles)     
     
END
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString]
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO
/****** Object:  Table [dbo].[ArtBookMark_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArtBookMark_tbl](
	[ab_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ab_UserId] [bigint] NULL,
	[ab_artwId] [bigint] NULL,
	[ab_CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_ArtBookMark_tbl] PRIMARY KEY CLUSTERED 
(
	[ab_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArtistProfile_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArtistProfile_tbl](
	[arts_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[arts_name] [varchar](50) NOT NULL,
	[arts_photo] [varchar](200) NULL,
	[arts_info] [varchar](max) NULL,
	[arts_status] [bit] NULL,
	[arts_createdBy] [bigint] NULL,
	[arts_creadtedDate] [datetime] NULL,
	[arts_ModifyBy] [bigint] NULL,
	[arts_ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_ArtistProfile_tbl] PRIMARY KEY CLUSTERED 
(
	[arts_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArtVisited_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArtVisited_tbl](
	[av_id] [bigint] IDENTITY(1,1) NOT NULL,
	[av_UserId] [bigint] NOT NULL,
	[av_artwId] [bigint] NOT NULL,
	[av_createdDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ArtVisited_tbl] PRIMARY KEY CLUSTERED 
(
	[av_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArtWorkProfile_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArtWorkProfile_tbl](
	[artw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[artw_Name] [varchar](50) NULL,
	[artw_image] [varchar](200) NOT NULL,
	[artw_arts_id] [bigint] NULL,
	[artw_arts_Name] [varchar](50) NULL,
	[artw_Info] [varchar](max) NULL,
	[artw_size] [varchar](50) NULL,
	[atrw_age] [varchar](50) NULL,
	[artw_latitude] [varchar](50) NULL,
	[artw_longitude] [varchar](50) NULL,
	[artw_address] [varchar](200) NULL,
	[artw_status] [int] NULL,
	[artw_statusNotes] [varchar](200) NULL,
	[artw_createdby] [bigint] NULL,
	[artw_createdDate] [datetime] NULL,
	[artw_modifyBy] [bigint] NULL,
	[artw_modifyDate] [datetime] NULL,
 CONSTRAINT [PK_ArtWorkProfile_tbl] PRIMARY KEY CLUSTERED 
(
	[artw_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog_tbl](
	[bId] [bigint] IDENTITY(1,1) NOT NULL,
	[bTitle] [varchar](50) NOT NULL,
	[bImage] [varchar](200) NULL,
	[bStatus] [bit] NULL,
	[bCreatedBy] [bigint] NULL,
	[bCreatedDate] [datetime] NULL,
	[bModifyBy] [int] NULL,
	[bModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Blog_tbl] PRIMARY KEY CLUSTERED 
(
	[bId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blogcomment_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blogcomment_tbl](
	[bc_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[bc_BlogId] [bigint] NULL,
	[bc_text] [varchar](500) NULL,
	[bc_UserId] [bigint] NULL,
	[bc_masterId] [bigint] NULL,
	[bc_CreatedDate] [datetime] NULL,
	[bc_ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Blogcomment_tbl] PRIMARY KEY CLUSTERED 
(
	[bc_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogcommentLike_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogcommentLike_tbl](
	[bcl_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[bcl_bcId] [bigint] NULL,
	[bcl_UserId] [bigint] NULL,
	[bcl_CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_BlogcommentLike_tbl] PRIMARY KEY CLUSTERED 
(
	[bcl_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogDetail_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogDetail_tbl](
	[bd_id] [bigint] IDENTITY(1,1) NOT NULL,
	[bd_BlogId] [bigint] NULL,
	[bd_type] [int] NULL,
	[bd_Value] [varchar](max) NULL,
	[bd_order] [int] NULL,
	[bd_createdBy] [bigint] NULL,
	[bd_createdDate] [datetime] NULL,
	[bd_ModifyBy] [bigint] NULL,
	[bd_ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_BlogDetail_tbl] PRIMARY KEY CLUSTERED 
(
	[bd_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogLike_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogLike_tbl](
	[bl_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[bl_BlogId] [bigint] NOT NULL,
	[bl_UserId] [bigint] NOT NULL,
	[bl_CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_BlogLike_tbl] PRIMARY KEY CLUSTERED 
(
	[bl_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ELMAH_Error]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL,
	[Application] [nvarchar](60) NOT NULL,
	[Host] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Source] [nvarchar](60) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[User] [nvarchar](50) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[TimeUtc] [datetime] NOT NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Suggestion_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suggestion_tbl](
	[s_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[s_text] [varchar](500) NULL,
	[s_UserId] [bigint] NULL,
	[s_CreatedDate] [datetime] NULL,
	[s_ModifyDate] [datetime] NULL,
	[s_status] [bit] NULL,
 CONSTRAINT [PK_Suggestion_tbl] PRIMARY KEY CLUSTERED 
(
	[s_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TagartShop_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagartShop_tbl](
	[ts_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ts_Title] [varchar](50) NULL,
	[ts_size] [varchar](50) NULL,
	[ts_Price] [varchar](15) NULL,
	[ts_description] [varchar](500) NULL,
	[ts_Email] [varchar](250) NULL,
	[ts_phone] [varchar](15) NULL,
	[ts_type] [bit] NULL,
	[ts_Letitude] [varchar](50) NULL,
	[ts_Logitude] [varchar](50) NULL,
	[ts_Address] [varchar](200) NULL,
	[ts_UserId] [bigint] NULL,
	[ts_Status] [int] NULL,
	[ts_createdDate] [datetime] NULL,
	[ts_ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_TagartShop_tbl] PRIMARY KEY CLUSTERED 
(
	[ts_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TagartShopImage_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagartShopImage_tbl](
	[tsi_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[tsi_tshopId] [bigint] NULL,
	[tsi_Image] [varchar](250) NULL,
 CONSTRAINT [PK_TagartShopImage_tbl] PRIMARY KEY CLUSTERED 
(
	[tsi_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TagartShopReport_tbl]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagartShopReport_tbl](
	[tsr_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[tsr_tsId] [bigint] NULL,
	[tsr_Description] [varchar](500) NULL,
	[tsr_UserId] [bigint] NULL,
	[tsr_createdDate] [datetime] NULL,
	[tsr_status] [bit] NULL,
 CONSTRAINT [PK_TagartShopReport_tbl] PRIMARY KEY CLUSTERED 
(
	[tsr_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserList]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserList](
	[usr_UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[usr_FullName] [varchar](50) NULL,
	[usr_UserName] [varchar](50) NULL,
	[usr_UserEmail] [varchar](250) NOT NULL,
	[usr_UserPassword] [varchar](50) NULL,
	[usr_LoginType] [int] NULL,
	[usr_Status] [bit] NULL,
	[usr_Token] [varchar](max) NULL,
	[usr_Image] [varchar](200) NULL,
	[usr_CreatedDate] [datetime] NULL,
	[usr_UpdatedDate] [datetime] NULL,
	[usr_IsLoggedIn] [bit] NULL,
	[usr_isforget] [bit] NULL,
	[usr_Role] [int] NULL,
 CONSTRAINT [PK_UserList] PRIMARY KEY CLUSTERED 
(
	[usr_UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ArtBookMark_tbl] ON 

INSERT [dbo].[ArtBookMark_tbl] ([ab_id], [ab_UserId], [ab_artwId], [ab_CreatedDate]) VALUES (1, 1, 2, NULL)
INSERT [dbo].[ArtBookMark_tbl] ([ab_id], [ab_UserId], [ab_artwId], [ab_CreatedDate]) VALUES (2, 2, 3, NULL)
SET IDENTITY_INSERT [dbo].[ArtBookMark_tbl] OFF
SET IDENTITY_INSERT [dbo].[ArtistProfile_tbl] ON 

INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (2, N'Pramod Kurlekar
', N'mahesh_201909231842131.jpg', N'The artist, originally from Rajasthan is settled in Tamilnadu. Hailing from a family of cloth merchants, he studied commerce and stepped into the family business. However, the artist in him since childhood grew exponentially and he finally took the brush seriously.', 1, 1, CAST(N'2019-09-23T18:42:35.057' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (3, N'Shyamal Mukherjee', N'1444024111_harvey-2.jpg', N'Shyamal Mukherjee is an artist who has spent his entire life in Calcutta, only venturing out to attend showings of his works, and only speaking in Bengali. He was born there in 1961, and spent all of his college years in Santiniketan, first completing his BFA in 1987, and then his MFA in 1989 from the Rabindra Bharati University.', 1, 1, CAST(N'2019-09-23T18:42:35.057' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (4, N'Yasala Balaiah', N'team-member-1.jpg', N'Yasala Balaiah an established senior artist from Ibrahimpur was born in 1939 & now resides in Hyderabad. After completing his M. A. & B. Ed in Drawing & Painting in 1962 he took up a job as an art teacher. This multi talented artist, who is also an expert at Batik painting, at one time earned himself the nick name, â€œBatik Balaiahâ€, fascinating people all over the world with his dexterous experiments in this medium.', 1, 1, CAST(N'2019-09-23T18:42:35.057' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (5, N'Raju Deshpande
', N'user-profile-5.jpg', N'Raju Deshpande was born in 1961 in Pune and completed his G.D. Art from Abhinav Mahavidyalaya in Pune in the year 1981. Beside which he honed his skills under the guidance of senior artist Subhash Awchat, in his studio for over 5 years.', 1, 1, CAST(N'2019-09-23T18:42:35.057' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (6, N'Neeraj Goswami
', N'people19.png', N'Art of Neeraj Goswami reflects the state of the art in India as it grew over the last fifty years. Born in Patna, in 1964, he grew up in Delhi since the age of eight and was groomed to be an artist since then even though his family had little means to support such an ambition. He was honoured with a solo show in his school days in Delhi and won considerable attention from veterans like Saradindu Sen Roy and Gulam Rasool Santosh before entering Art College in Delhi.', 1, 1, CAST(N'2019-09-23T18:42:35.057' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (7, N'mahesh', N'mahesh_201910021541253.jpg', N'this is testing data', 1, 1, CAST(N'2019-10-02T15:41:25.460' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtistProfile_tbl] ([arts_Id], [arts_name], [arts_photo], [arts_info], [arts_status], [arts_createdBy], [arts_creadtedDate], [arts_ModifyBy], [arts_ModifyDate]) VALUES (8, N'rajesh123', N'rajesh123_201910021555594.jpg', N'this is testing data', 1, 1, CAST(N'2019-10-02T15:55:59.543' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[ArtistProfile_tbl] OFF
SET IDENTITY_INSERT [dbo].[ArtVisited_tbl] ON 

INSERT [dbo].[ArtVisited_tbl] ([av_id], [av_UserId], [av_artwId], [av_createdDate]) VALUES (2, 1, 2, CAST(N'2019-10-03T00:00:00.000' AS DateTime))
INSERT [dbo].[ArtVisited_tbl] ([av_id], [av_UserId], [av_artwId], [av_createdDate]) VALUES (3, 2, 3, CAST(N'2019-09-01T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[ArtVisited_tbl] OFF
SET IDENTITY_INSERT [dbo].[ArtWorkProfile_tbl] ON 

INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (2, N'mahesh', N'0_2019092413131972.png', 2, NULL, N'this is testind data', N'12x 12', N'1', N'18.5514031', N'73.9337138', N'PUNE', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (7, N'rakesh', N'streetart-88@2x.png', 3, NULL, N'test', N'10x12', N'2', N'23.019273', N'72.527726', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (8, N'JAYESH', N'streetart-89@2x.png', 4, NULL, N'test', N'13X15', N'3', N'22.999692', N'72.612016', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (9, N'abc', N'streetart-90@2x.png', 5, NULL, N'test', N'11X13', N'2', N'22.994358', N'72.499144', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10008, N'asd', N'streetart-92@2x.png', 6, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 4, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10009, N'asd', N'streetart-99@2x.png', 2, NULL, N'test', N'13X15', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10010, N'sad', N'streetart-103@2x.png', 3, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10011, N'BILALAfdsf', N'streetart-106@2x.png', 4, NULL, N'test', N'10x12', N'1', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10012, N'sdfdf', N'streetart-92@2x.png', 5, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10013, N'BILALA', N'streetart-107@2x.png', 6, NULL, N'test', N'13X15', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10014, N'BILALA', N'streetart-108@2x.png', 2, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10015, N'BILALA', N'streetart-99@2x.png', 3, NULL, N'test', N'11X13', N'2', N'22.994358', N'72.499144', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10016, N'BILALA', N'streetart-154@2x.png', 4, NULL, N'test', N'10x12', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10017, N'BILALA', N'streetart-103@2x.png', 5, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 2, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10018, N'BILALA', N'streetart-157@2x.png', 6, NULL, N'test', N'13X15', N'2', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10019, N'BILALA', N'streetart-108@2x.png', 3, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10020, N'BILALA', N'streetart-92@2x.png', 2, NULL, N'test', N'10x12', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10021, N'BILALA', N'streetart-158@2x.png', 5, NULL, N'test', N'11X13', N'1', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10022, N'BILALA', N'streetart-99@2x.png', 4, NULL, N'test', N'11X13', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10023, N'BILALA', N'streetart-159@2x.png', 5, NULL, N'test', N'10x12', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10024, N'BILALA', N'streetart-92@2x.png', 3, NULL, N'test', N'11X13', N'1', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10025, N'BILALA', N'streetart-106@2x.png', 2, NULL, N'test', N'10x12', N'3', N'22.994358', N'72.499144', N'AHMEDABAD', 3, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10026, N'BILALA', N'streetart-160@2x.png', 6, NULL, N'test', N'11X13', N'2', N'22.994358', N'72.499144', N'AHMEDABAD', 1, NULL, 1, CAST(N'2019-09-24T13:19:32.270' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10027, N'jaimin', N'streetart-183@2x.png', 5, NULL, N'test', N'13X15', N'1', N'22.994258', N'72.488244', N'rajkot', 1, NULL, 1, CAST(N'2019-09-24T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[ArtWorkProfile_tbl] ([artw_id], [artw_Name], [artw_image], [artw_arts_id], [artw_arts_Name], [artw_Info], [artw_size], [atrw_age], [artw_latitude], [artw_longitude], [artw_address], [artw_status], [artw_statusNotes], [artw_createdby], [artw_createdDate], [artw_modifyBy], [artw_modifyDate]) VALUES (10028, N'mahesh', N'0_201911041141477.jpg', NULL, N'M. F. Husain', N'this is testind data', N'12x 12', N'1', N'', N'', N'makarbag, ahmedabad', 1, NULL, 0, CAST(N'2019-11-04T11:42:01.633' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[ArtWorkProfile_tbl] OFF
SET IDENTITY_INSERT [dbo].[Blog_tbl] ON 

INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (1, N'The ''Best'' Headlines', N'streetart-89@2x.png', 1, 1, CAST(N'2019-09-30T00:00:00.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (2, N'this is testing data for Blog', N'Blog_T_201910011803436.png', 1, 1, CAST(N'2019-10-01T18:04:34.563' AS DateTime), NULL, NULL)
INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (3, N'testing', N'Blog_T_201910011824013.jpg', 1, 1, CAST(N'2019-10-01T18:24:02.803' AS DateTime), NULL, NULL)
INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (4, N'create blog', N'Blog_T_201910011848527.png', 1, 0, CAST(N'2019-10-01T18:49:20.307' AS DateTime), NULL, NULL)
INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (5, N'this month top 5 Tag art list', N'Blog_T_201910011856563.jpg', 1, 1, CAST(N'2019-10-01T18:56:57.117' AS DateTime), NULL, NULL)
INSERT [dbo].[Blog_tbl] ([bId], [bTitle], [bImage], [bStatus], [bCreatedBy], [bCreatedDate], [bModifyBy], [bModifyDate]) VALUES (6, N'test 1', N'Blog_T_201910011957211.png', 1, 1, CAST(N'2019-10-01T19:57:21.873' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Blog_tbl] OFF
SET IDENTITY_INSERT [dbo].[BlogDetail_tbl] ON 

INSERT [dbo].[BlogDetail_tbl] ([bd_id], [bd_BlogId], [bd_type], [bd_Value], [bd_order], [bd_createdBy], [bd_createdDate], [bd_ModifyBy], [bd_ModifyDate]) VALUES (3, 1, 1, N'<p>sadsa dosa doas dsad</p>

<p>sad</p>

<p>sa</p>

<p>dsad</p>

<p>sad</p>

<p>sad</p>

<p>sad</p>

<p>s</p>

<p>ad</p>

<p>sa</p>

<p>&nbsp;</p>

<p><img alt="" src="https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=3&amp;h=750&amp;w=1260" style="height:132px; width:200px" /></p>
', 1, 1, CAST(N'2019-10-02T14:46:33.480' AS DateTime), 1, CAST(N'2019-10-25T20:56:44.833' AS DateTime))
INSERT [dbo].[BlogDetail_tbl] ([bd_id], [bd_BlogId], [bd_type], [bd_Value], [bd_order], [bd_createdBy], [bd_createdDate], [bd_ModifyBy], [bd_ModifyDate]) VALUES (4, 1, 1, N'<p>dsfdsfdsfdsfsdfds</p>

<p>fdsfsdfdsfdsf dssd fdsfjldsfjldsf ldsf jldsjfldsf lsdfl ds flds fldsjfl dsld sfl dfl fdslfldsf dslf ldsf dsf</p>
', 2, 1, CAST(N'2019-10-02T14:47:06.900' AS DateTime), NULL, NULL)
INSERT [dbo].[BlogDetail_tbl] ([bd_id], [bd_BlogId], [bd_type], [bd_Value], [bd_order], [bd_createdBy], [bd_createdDate], [bd_ModifyBy], [bd_ModifyDate]) VALUES (5, 1, 1, N'<p>adfsasd lsadlsads</p>

<p>ad</p>

<p>sadsadsad sad&nbsp;</p>
', 3, 1, CAST(N'2019-10-02T19:57:55.957' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[BlogDetail_tbl] OFF
SET IDENTITY_INSERT [dbo].[TagartShop_tbl] ON 

INSERT [dbo].[TagartShop_tbl] ([ts_Id], [ts_Title], [ts_size], [ts_Price], [ts_description], [ts_Email], [ts_phone], [ts_type], [ts_Letitude], [ts_Logitude], [ts_Address], [ts_UserId], [ts_Status], [ts_createdDate], [ts_ModifyDate]) VALUES (1, N'test1', N'12x12', N'2500', N'this is testing ', N'shinevishal@gmail.com', N'9874563210', 0, N'27.45217', N'72.478541', N'ahmedabad, gujarat', 1, 1, CAST(N'2019-11-02T19:47:19.053' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[TagartShop_tbl] OFF
SET IDENTITY_INSERT [dbo].[TagartShopImage_tbl] ON 

INSERT [dbo].[TagartShopImage_tbl] ([tsi_Id], [tsi_tshopId], [tsi_Image]) VALUES (1, 1, N'1_201911021947137.jpg')
INSERT [dbo].[TagartShopImage_tbl] ([tsi_Id], [tsi_tshopId], [tsi_Image]) VALUES (2, 1, N'2_201911021947176.jpg')
SET IDENTITY_INSERT [dbo].[TagartShopImage_tbl] OFF
SET IDENTITY_INSERT [dbo].[UserList] ON 

INSERT [dbo].[UserList] ([usr_UserId], [usr_FullName], [usr_UserName], [usr_UserEmail], [usr_UserPassword], [usr_LoginType], [usr_Status], [usr_Token], [usr_Image], [usr_CreatedDate], [usr_UpdatedDate], [usr_IsLoggedIn], [usr_isforget], [usr_Role]) VALUES (1, N'admin', N'admin', N'admin@gmail.com', N'123', 3, 1, N'Email', NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[UserList] ([usr_UserId], [usr_FullName], [usr_UserName], [usr_UserEmail], [usr_UserPassword], [usr_LoginType], [usr_Status], [usr_Token], [usr_Image], [usr_CreatedDate], [usr_UpdatedDate], [usr_IsLoggedIn], [usr_isforget], [usr_Role]) VALUES (2, N'harish', N'sakhra', N'abc@gmail.com', N'123', 1, 1, N'facebook', NULL, CAST(N'2019-09-23T15:18:55.590' AS DateTime), NULL, 1, NULL, 3)
INSERT [dbo].[UserList] ([usr_UserId], [usr_FullName], [usr_UserName], [usr_UserEmail], [usr_UserPassword], [usr_LoginType], [usr_Status], [usr_Token], [usr_Image], [usr_CreatedDate], [usr_UpdatedDate], [usr_IsLoggedIn], [usr_isforget], [usr_Role]) VALUES (3, N'bilal', N'chaudhry', N'abcd@gmail.com', N'123', 3, 1, NULL, N'bilal_201909251517085.jpg', CAST(N'2019-09-23T15:54:13.703' AS DateTime), CAST(N'2019-09-25T15:17:18.607' AS DateTime), 1, NULL, 3)
INSERT [dbo].[UserList] ([usr_UserId], [usr_FullName], [usr_UserName], [usr_UserEmail], [usr_UserPassword], [usr_LoginType], [usr_Status], [usr_Token], [usr_Image], [usr_CreatedDate], [usr_UpdatedDate], [usr_IsLoggedIn], [usr_isforget], [usr_Role]) VALUES (4, N'harish', N'gadhavi', N'shinevishal11@gmail.com', N'123', 3, 1, NULL, NULL, CAST(N'2019-10-07T11:49:13.070' AS DateTime), NULL, 0, 0, 3)
INSERT [dbo].[UserList] ([usr_UserId], [usr_FullName], [usr_UserName], [usr_UserEmail], [usr_UserPassword], [usr_LoginType], [usr_Status], [usr_Token], [usr_Image], [usr_CreatedDate], [usr_UpdatedDate], [usr_IsLoggedIn], [usr_isforget], [usr_Role]) VALUES (10010, N'Hitesh', N'B', N'Hitesh@gmail.com', N'123', 3, 1, NULL, NULL, CAST(N'2019-10-25T19:43:30.370' AS DateTime), NULL, 0, NULL, 2)
SET IDENTITY_INSERT [dbo].[UserList] OFF
/****** Object:  Index [PK_ELMAH_Error]    Script Date: 04-11-2019 5:06:09 PM ******/
ALTER TABLE [dbo].[ELMAH_Error] ADD  CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ArtistProfile_tbl] ADD  CONSTRAINT [DF_ArtistProfile_tbl_arts_status]  DEFAULT ((1)) FOR [arts_status]
GO
ALTER TABLE [dbo].[ArtWorkProfile_tbl] ADD  CONSTRAINT [DF_ArtWorkProfile_tbl_artw_status]  DEFAULT ((1)) FOR [artw_status]
GO
ALTER TABLE [dbo].[Blog_tbl] ADD  CONSTRAINT [DF_Blog_tbl_bStatus]  DEFAULT ((1)) FOR [bStatus]
GO
ALTER TABLE [dbo].[ELMAH_Error] ADD  CONSTRAINT [DF_ELMAH_Error_ErrorId]  DEFAULT (newid()) FOR [ErrorId]
GO
/****** Object:  StoredProcedure [dbo].[ArtStatusUpdate_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ArtStatusUpdate_sp]
@artw_id bigint=null,
@artw_status int=null,
@Action int 

AS
BEGIN
	IF(@Action=1)
	BEGIN
		update ArtWorkProfile_tbl set artw_status=@artw_status where artw_id=@artw_id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[BlogDetail_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BlogDetail_sp]
@bd_id INT=NULL,
@bd_BlogId bigint=null,
@Action int
AS
BEGIN
	If(@Action=1)
	BEGIN
		SELECT *  FROM BlogDetail_tbl WHERE bd_BlogId=@bd_BlogId order by bd_order
	END

	IF(@Action=2)
	BEGIN
	SELECT *  FROM BlogDetail_tbl WHERE bd_id=@bd_id
	END
	IF(@Action=3)
	BEGIN
	SELECT *  FROM BlogDetail_tbl
	END

END
GO
/****** Object:  StoredProcedure [dbo].[ChangePassword_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChangePassword_sp]
@usr_UserId varchar(250) = NULL,
@usr_UserPassword varchar(50) = NULL
AS
BEGIN
	Update UserList Set usr_UserPassword = @usr_UserPassword, usr_isforget = 0 where usr_UserId=@usr_UserId
	Select 1
END
GO
/****** Object:  StoredProcedure [dbo].[checkUserEmail]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- 
CREATE PROCEDURE [dbo].[checkUserEmail]
@usr_UserEmail varchar(250),
@usr_UserId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	if(@usr_UserId>0)
		BEGIN
				SELECT count(*)AS CHECKNAME from UserList where usr_UserEmail=@usr_UserEmail and usr_UserId != @usr_UserId
		END
	ELSE
		BEGIN
				SELECT count(*)AS CHECKNAME from UserList where usr_UserEmail=@usr_UserEmail 
		END
	
  
END
GO
/****** Object:  StoredProcedure [dbo].[FindAccount_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FindAccount_sp]
@Action varchar(50) = NULL,
@usr_UserEmail varchar(250) = NULL,
@usr_UserPassword varchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	if(@Action = '1')
	BEGIN
		SELECT * from UserList where usr_UserEmail=@usr_UserEmail

		if(SELECT COUNT(*) from UserList where usr_UserEmail=@usr_UserEmail) > 0
		BEGIN
			Update UserList Set usr_isforget = 1 where usr_UserEmail=@usr_UserEmail
		END
	END

	if(@Action = '2')
	BEGIN
		SELECT usr_UserId As StatusResult from UserList where usr_UserEmail=@usr_UserEmail
	END
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllArtWorkProfile_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec GetArtistProfile_sp null,'raj',3  
CREATE  PROCEDURE [dbo].[GetAllArtWorkProfile_sp]  
@artw_id bigint =null,  
@artw_Name varchar(50)=null,  
@artw_arts_id int =null, 
@artw_arts_Name varchar(50)=null, 
@atrw_age  int=null,  
@artw_latitude_current  varchar(50)=null,  
@artw_longitude_current   varchar(50)=null,  
@index int=null,  
@User_Id bigint=null,
@Action int  
  
AS  
BEGIN  
 IF(@Action=1)-- GET ALL   
 BEGIN  
 
 -- SELECT TOP (50) PERCENT * FROM ArtWorkProfile_tbl WHERE artw_status=1 AND   
  SELECT *,((dbo.Distance(artw_latitude, artw_longitude, @artw_latitude_current,  @artw_longitude_current))*1.6093) AS km,
  isnull((select top 1 ab_id from ArtBookMark_tbl where ab_artwId=artw_id and ab_UserId=@User_Id),0) as BookMark,
  isnull((select  top 1 av_id from ArtVisited_tbl where av_artwId=artw_id and av_UserId=@User_Id),0)  as Visited  
   FROM ArtWorkProfile_tbl WHERE artw_status=1  ORDER BY km   
   --  OFFSET @index ROWS FETCH NEXT 20 ROWS ONLY;  
  
  
 END  
  
 IF(@Action=2)-- GET by Id   
 BEGIN  
  
  SELECT *, 
   isnull((select top 1 ab_id from ArtBookMark_tbl where ab_artwId=artw_id and ab_UserId=@User_Id),0) as BookMark,
  isnull((select  top 1 av_id from ArtVisited_tbl where av_artwId=artw_id and av_UserId=@User_Id),0)  as Visited  
  FROM ArtWorkProfile_tbl WHERE artw_status=1  and artw_id=@artw_id  
 END  
  
 IF(@Action=3)-- GET by Name   
 BEGIN  
 SELECT *,((dbo.Distance(artw_latitude, artw_longitude, @artw_latitude_current,  @artw_longitude_current))*1.6093) AS km,
  isnull((select top 1 ab_id from ArtBookMark_tbl where ab_artwId=artw_id and ab_UserId=@User_Id),0) as BookMark,
  isnull((select  top 1 av_id from ArtVisited_tbl where av_artwId=artw_id and av_UserId=@User_Id),0)  as Visited  
   FROM ArtWorkProfile_tbl WHERE artw_status=1 and artw_arts_Name like @artw_arts_Name+'%' ORDER BY km   
  
 -- SELECT * FROM ArtWorkProfile_tbl WHERE artw_status=1 and artw_Name like @artw_Name+'%'  
 END  
   if(@Action =4) --get artist wise artwork 
   BEGIN 
   SELECT *,
    isnull((select top 1 ab_id from ArtBookMark_tbl where ab_artwId=artw_id and ab_UserId=@User_Id),0) as BookMark,
  isnull((select  top 1 av_id from ArtVisited_tbl where av_artwId=artw_id and av_UserId=@User_Id),0)  as Visited  
 
     FROM ArtWorkProfile_tbl WHERE artw_arts_id=@artw_arts_id and artw_status!=4   order by artw_Name  OFFSET @index ROWS FETCH NEXT 8 ROWS ONLY;
   

   END
    if(@Action =5) --all 
   BEGIN 

   SELECT artw_id,artw_Name,artw_image,artw_Info,artw_size, atrw_age,artw_longitude,artw_longitude,artw_address,artw_status,artw_arts_id,
   artw_arts_Name as arts_name ,
   --(select arts_name from ArtistProfile_tbl where arts_Id=artw_arts_id)as arts_name,
    (select arts_photo from ArtistProfile_tbl where arts_Id=artw_arts_id)as arts_photo
	, isnull((select top 1 ab_id from ArtBookMark_tbl where ab_artwId=artw_id and ab_UserId=@User_Id),0) as BookMark,
  isnull((select  top 1 av_id from ArtVisited_tbl where av_artwId=artw_id and av_UserId=@User_Id),0)  as Visited  
	 FROM ArtWorkProfile_tbl WHERE artw_status!=4    order by artw_Name OFFSET @index ROWS FETCH NEXT 8 ROWS ONLY;
   
   END
END  
GO
/****** Object:  StoredProcedure [dbo].[GetArtistProfile_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec GetArtistProfile_sp null,'raj',3
CREATE PROCEDURE [dbo].[GetArtistProfile_sp]
@arts_Id bigint =null,
@arts_name varchar(50)=null,
@Action int

AS
BEGIN

	IF(@Action=1)-- GET ALL 
	BEGIN
		SELECT * FROM ArtistProfile_tbl WHERE arts_status=1
	END

	IF(@Action=2)-- GET by Id 
	BEGIN
		SELECT arts_Id, arts_name,arts_photo,arts_info,(select COUNT(artw_arts_id) from ArtWorkProfile_tbl where artw_arts_id=@arts_Id and artw_status !=4)as Upload FROM ArtistProfile_tbl WHERE arts_status=1 and arts_Id=@arts_Id group by arts_name,arts_photo,arts_info,arts_Id 

	END

	IF(@Action=3)-- GET by Name 
	BEGIN
		SELECT * FROM ArtistProfile_tbl WHERE arts_status=1 and arts_name like @arts_name+'%'
	END


END
GO
/****** Object:  StoredProcedure [dbo].[GetBlog_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBlog_sp]
@bId bigint=null,
@index INT=NULL,
@Action int
AS
BEGIN
	If(@Action=1)
	BEGIN
		SELECT *  FROM Blog_tbl WHERE bStatus=1 ORDER BY bCreatedDate OFFSET @index ROWS FETCH NEXT 8 ROWS ONLY;
	END

	IF(@Action=2)
	BEGIN
	SELECT *  FROM Blog_tbl WHERE bId=@bId
	END

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserlogin_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
--GetUserlogin_sp 'admin@gmail.com','123'
CREATE PROCEDURE [dbo].[GetUserlogin_sp]
	@usr_UserEmail varchar(250),
	@usr_UserPassword varchar(50),
	@usr_Token varchar(MAX)=null,
	@usr_LoginType int=null

AS
BEGIN
			if(@usr_LoginType=3)
				begin

					select * from  UserList where usr_UserEmail=@usr_UserEmail and usr_UserPassword=@usr_UserPassword and usr_Status=1
				END 
			ELSE
				begin
					select * from  UserList where usr_UserEmail=@usr_UserEmail and usr_Status=1
	
				end
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Delete_ArtBookMark_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Delete_ArtBookMark_sp]
@ab_id bigint =null,
@ab_UserId bigint =null,
@ab_artwId bigint=null,
@Action int


AS
BEGIN
	IF(@Action=1)
	BEGIN
			INSERT INTO ArtBookMark_tbl(
				
				ab_UserId
				,ab_artwId
				,ab_CreatedDate
			) VALUES
			(
				
				@ab_UserId
				,@ab_artwId
				,GETDATE()
			)
			SELECT CAST(SCOPE_IDENTITY() AS INT) 
			
	END
	IF(@Action=2)
	BEGIN
			DELETE FROM ArtBookMark_tbl WHERE ab_id=@ab_id
		select 1
	END
	

END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Delete_ArtVisited_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Insert_Delete_ArtVisited_sp]
@av_id bigint =null,
@av_UserId bigint =null,
@av_artwId bigint=null,
@Action int

				

AS
BEGIN
	IF(@Action=1)
	BEGIN
			INSERT INTO ArtVisited_tbl(
					av_UserId
					,av_artwId
					,av_createdDate

			) VALUES
			(
				@av_UserId
				,@av_artwId
				,GETDATE()
			)
	END
	IF(@Action=2)
	BEGIN
			DELETE FROM ArtVisited_tbl WHERE av_id=@av_id
	END


END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_ArtistProfile_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Update_ArtistProfile_sp]
@arts_Id bigint=null,
@arts_name varchar(50)=null,
@arts_photo varchar(200)=null,
@arts_info  varchar(50)=null,
@arts_status  bit=null,
@arts_createdBy bigint =null,
@arts_ModifyBy  bigint =null,
@Action int	
AS
BEGIN
	IF(@Action=1)
	BEGIN
	INSERT INTO ArtistProfile_tbl(
			arts_name
			,arts_photo
			,arts_info
			,arts_status
			,arts_createdBy
			,arts_creadtedDate
			
		) 
			VALUES(
			
			@arts_name
			,@arts_photo
			,@arts_info
			,@arts_status
			,@arts_createdBy
			,GETDATE()
			
			)

	END
	IF(@Action=2)
	BEGIN
	UPDATE ArtistProfile_tbl SET 
			arts_name=@arts_name
			,arts_photo=@arts_photo
			,arts_info=@arts_info
			,arts_status=@arts_status
			,arts_ModifyBy=@arts_ModifyBy
			,arts_ModifyDate=GETDATE()
			
		 WHERE arts_Id=@arts_Id
	END
	IF(@Action=3)
	BEGIN
		DELETE FROM  ArtistProfile_tbl WHERE arts_Id=@arts_Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_ArtWorkProfile_tbl_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[Insert_Update_ArtWorkProfile_tbl_sp]
@artw_id INT=NULL,
@artw_Name VARCHAR(50)=NULL,
@artw_image VARCHAR(200)=NULL,
@artw_arts_id BIGINT=NULL,
@artw_arts_Name varchar(50)=null,
@artw_Info VARCHAR(MAX)=NULL,
@artw_size VARCHAR(50)=NULL,
@atrw_age varchar(5)=null,
@artw_latitude VARCHAR(50)=NULL,
@artw_longitude VARCHAR(50)=NULL,
@artw_address VARCHAR(200)=NULL,
@artw_status int=NULL,
@artw_statusNotes VARCHAR(200)=NULL,
@artw_createdby INT=NULL,
@artw_modifyBy INT=NULL,
@Action int

	

AS
BEGIN
	IF(@Action=1)
	BEGIN
			INSERT INTO ArtWorkProfile_tbl(		
				--	artw_Name
					artw_image
					
					,artw_arts_Name
					,artw_Info
					,artw_size
					,atrw_age
					,artw_latitude
					,artw_longitude
					,artw_address
					,artw_status
					,artw_statusNotes
					,artw_createdby
					,artw_createdDate		
					)
					VALUES
					(
					--@artw_Name
					@artw_image
					
					,@artw_arts_Name
					,@artw_Info
					,@artw_size
					,@atrw_age
					,@artw_latitude
					,@artw_longitude
					,@artw_address
					,@artw_status
					,@artw_statusNotes
					,@artw_createdby
					,GETDATE()	
					)

	END
	IF(@Action=2)
	BEGIN
	
			UPDATE [dbo].[ArtWorkProfile_tbl]
			   SET
				--  [artw_Name] = @artw_Name
				  [artw_image] = @artw_image
				 -- ,[artw_arts_id] = @artw_arts_id
				 ,artw_arts_Name=@artw_arts_Name
				  ,[artw_Info] = @artw_size
				  ,[artw_size] = @artw_size
				  ,atrw_age= @atrw_age
				  ,[artw_latitude] = @artw_latitude
				  ,[artw_longitude] = @artw_longitude
				  ,[artw_address] = @artw_address
				  ,[artw_status] = @artw_status
				  ,[artw_statusNotes] =@artw_statusNotes
			
				  ,[artw_modifyBy] = @artw_modifyBy
				  ,[artw_modifyDate] = GETDATE()
			 WHERE artw_id=@artw_id
	END
	IF(@Action=3)
	BEGIN
	DELETE FROM ArtWorkProfile_tbl WHERE artw_id=@artw_id
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_Blog_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Update_Blog_sp]
@bId bigint=null,
@bTitle varchar(50)=null,
@bImage varchar(200)=null,
@bStatus bit=null,
@bCreatedBy bigint=null,
@bModifyBy bigint=null,
@Action INT

AS
BEGIN
		IF(@Action=1)
		BEGIN
			INSERT INTO Blog_tbl(			
			bTitle
			,bImage
			,bStatus
			,bCreatedBy
			,bCreatedDate
			
		)VALUES(
			
			@bTitle
			,@bImage
			,@bStatus
			,@bCreatedBy
			,GETDATE()			
		)
				
		END
		IF(@Action=2)
		BEGIN
			UPDATE Blog_tbl SET 	
				bTitle=@bTitle
				,bImage=@bImage
				,bStatus=@bStatus
				,bModifyBy=@bModifyBy
				,bModifyDate=GETDATE()	
			WHERE bId=@bId

		END

		IF(@Action=3)
		BEGIN
		delete from Blog_tbl where bId=@bId
		END
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_BlogDetail_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Update_BlogDetail_sp]
@bd_id BIGINT=NULL,
@bd_BlogId BIGINT=NULL,
@bd_type INT=NULL,
@bd_Value VARCHAR(MAX)=NULL,
@bd_order INT=NULL,
@bd_createdBy BIGINT=NULL,
@bd_ModifyBy BIGINT=NULL,
@Action INT

AS
BEGIN
	IF(@Action=1)
	BEGIN

	declare  @last_order int
	set @last_order=(isnull((select MAX(bd_order) from BlogDetail_tbl where bd_BlogId=bd_BlogId ) ,0))+1
	
		INSERT INTO BlogDetail_tbl
	   (
		bd_BlogId
		,bd_type
		,bd_Value
		,bd_order
		,bd_createdBy
		,bd_createdDate
		
		)
		VALUES(
		 @bd_BlogId
		,@bd_type
		,@bd_Value
		,@last_order
		,@bd_createdBy
		,GETDATE()
		)
	END
	IF(@Action=2)
	BEGIN
		UPDATE BlogDetail_tbl SET 

		bd_BlogId=@bd_BlogId
		--,bd_type=@bd_type
		,bd_Value=@bd_Value
		--,bd_order=@bd_order
		,bd_ModifyBy=@bd_ModifyBy
		,bd_ModifyDate=GETDATE()
		WHERE bd_id=@bd_id


	END
	IF(@Action=3)
	BEGIN
		DELETE from BlogDetail_tbl WHERE bd_id=@bd_id
	END
	IF(@Action=4) --order update
	BEGIN
		UPDATE BlogDetail_tbl SET bd_order=@bd_order  from BlogDetail_tbl WHERE bd_id=@bd_id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_Delete_TagartShop_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [Insert_Update_Delete_TagartShop_sp] 0,'dsfkjdshfkjds','sdfds','dsfdsf','dfdsfdsfds','sdfdsfdsf','dsfdsfds',1,'sdfdsf','sdfdsf','sdfdsfdsf',
--0,0,'dfsdsfdsf,dsfdsfdsfdsf,dsfdsfdsfdsf,dsfdsfdsfds,dsfds',1
-- =============================================
CREATE PROCEDURE [dbo].[Insert_Update_Delete_TagartShop_sp]

@ts_Id int=null,	
@ts_Title varchar(50)=null,
@ts_size varchar(50)=null,
@ts_Price varchar(15)=null,
@ts_description	 varchar(500)=null,
@ts_Email varchar(250)=null,
@ts_phone varchar(15)=null,
@ts_type bit=null,
@ts_Letitude varchar(50)=null,
@ts_Logitude varchar(50)=null,
@ts_Address	varchar(200)=null,
@ts_UserId	bigint=null,
@ts_Status int=null,
@Images varchar(max)=null,
@Action int=null


AS
BEGIN
	IF(@Action=1)
	BEGIN
--	BEGIN TRAN T1;

	INSERT INTO TagartShop_tbl(
				ts_Title
				,ts_size
				,ts_Price
				,ts_description
				,ts_Email
				,ts_phone
				,ts_type
				,ts_Letitude
				,ts_Logitude
				,ts_Address
				,ts_UserId
				,ts_Status
				,ts_createdDate
	) VALUES(
				@ts_Title
				,@ts_size
				,@ts_Price
				,@ts_description
				,@ts_Email
				,@ts_phone
				,@ts_type
				,@ts_Letitude
				,@ts_Logitude
				,@ts_Address
				,@ts_UserId
				,@ts_Status
				,GETDATE()	
		)
			
		DECLARE @ID int;
		set @ID =ISNULL((select SCOPE_IDENTITY()),0)
		if(@ID!=0)
		--	begin
		--	--	COMMIT TRAN T1;  
		--	--	rollback TRAN T1;
		--	end
		--else
		begin
		--	COMMIT TRAN T1; 
			INSERT INTO TagartShopImage_tbl (tsi_tshopId,tsi_Image)
			SELECT @ID, Item FROM SplitString(@Images,',')
		end
			
--FROM dbo.SplitString('Apple,Mango,Banana,Guava', ',')

	END
END
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_UserList_sp]    Script Date: 04-11-2019 5:06:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Update_UserList_sp] 
@usr_UserId int =null,
@usr_FullName varchar(50)=null,
@usr_UserName varchar(50)=null,
@usr_UserEmail varchar(250)=null,
@usr_UserPassword varchar(50)=null,
@usr_LoginType int=null ,
@usr_Status bit =null,
@usr_Token varchar(max)=null,
@usr_Image varchar(200)=null,
@usr_CreatedDate datetime=null,
@usr_UpdatedDate datetime=null,
@usr_IsLoggedIn bit =null,
@usr_isforget bit=null,
@usr_Role int=null,
@Action int
AS
BEGIN
	IF(@Action=1)--CREATE
	BEGIN
			INSERT INTO UserList(
			usr_FullName
			,usr_UserName
			,usr_UserEmail
			,usr_UserPassword
			,usr_LoginType
			,usr_Status
			,usr_Token
			,usr_CreatedDate
			,usr_IsLoggedIn
			,usr_isforget
			,usr_Role)
				VALUES(
				@usr_FullName
			,@usr_UserName
			,@usr_UserEmail
			,@usr_UserPassword
			,@usr_LoginType
			,@usr_Status
			,@usr_Token
			,getdate()
			,@usr_IsLoggedIn
			,@usr_isforget
			,@usr_Role)
				SELECT CAST(SCOPE_IDENTITY() AS INT) 

	END
	IF(@Action=2)--UPDATE
	BEGIN
	UPDATE UserList SET 
				usr_FullName=@usr_FullName
			,usr_UserName=@usr_UserName
			,usr_Image=@usr_Image
			,usr_LoginType=@usr_LoginType	
			,usr_UpdatedDate=getdate()
			WHERE  usr_UserId=@usr_UserId
				
				
				SELECT 1



	END
	IF(@Action=3)--UPDATE
	BEGIN
		Select * from UserList Where usr_Role = 2
	END
END
GO
