﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TagArt_API.Utility;
using TagArt_Business.Model;

using TagArt_Business.Repository;
namespace TagArt_API.Models
{
    public class ArtworkDetailModel
    {
        public List<ArtWorkModel> ArtWork { get; set; }
        public List<BlogModel> Blog_model { get; set; }
        public List<BlogDetailModel> BlogDetail_model { get; set; }
    }
}