﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace TagArt_API.Utility
{
	public static class Helper
	{
		//public static JsonResponse GenerateJsonResponse(int HttpStatusCode, string message, object data = null)
		//{
		//    JsonResponse res = new JsonResponse();
		//    try
		//    {
		//        res.HttpStatusCode = HttpStatusCode;
		//        res.Message = message ?? string.Empty;
		//        res.data = data;
		//    }
		//    catch (Exception)
		//    {
		//        res.HttpStatusCode = HttpStatusCode;
		//        res.Message = message ?? string.Empty;
		//        res.data = data;
		//    }
		//    return res;
		//}

		public static JsonResponse GenerateErrorJsonResponse(string message, object data = null)
		{
			JsonResponse res = new JsonResponse();
			try
			{
				res.HttpStatusCode = (int)HttpStatusCode.BadRequest;
				res.Message = message ?? string.Empty;
				res.data = data ?? "";
			}
			catch (Exception)
			{
				res.HttpStatusCode = (int)HttpStatusCode.BadRequest;
				res.Message = message ?? string.Empty;
				res.data = "";
			}
			return res;
		}

		public static JsonResponse GenerateSuccessJsonResponse(string message, object data = null)
		{
			JsonResponse res = new JsonResponse();
			try
			{
				res.HttpStatusCode = (int)HttpStatusCode.OK;
				res.Message = message ?? string.Empty;
				res.data = data;
			}
			catch (Exception)
			{
				res.HttpStatusCode = (int)HttpStatusCode.OK;
				res.Message = message ?? string.Empty;
				res.data = data;
			}
			return res;
		}
	}
}