﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TagArt_API.Utility;
using TagArt_Business.Model;
using TagArt_Business.Repository;
using System.Threading.Tasks;

namespace TagArt_API.Controllers
{
    public class TagartShopController : ApiController
    {

         [Route("api/TagartShop/TagartShopeInsertUpdate")]
        [HttpPost]
        public async Task<IHttpActionResult> TagartShopeInsertUpdate(TagartShopModel model)
        {
            JsonResponse response = new JsonResponse();
            if (model != null)
            {

                if ((model.ts_Title == null || model.ts_Title == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Title name can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                if ((model.ts_size == null || model.ts_size == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Size can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                if ((model.ts_Price == null || model.ts_Price == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Price can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                if ((model.ts_description == null || model.ts_description == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Description can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                if ((model.ts_Email == null || model.ts_Email == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Email can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                if ((model.ts_phone == null || model.ts_phone == ""))
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Phone can not be null.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
                string allimagename = "";
                int cnt =0;
                foreach(var idata in model.ImageList)
                {
                    cnt = cnt + 1;
                    String path = HttpContext.Current.Server.MapPath("~/ImageStorage/TagartShop"); //Path
                    String path15 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_TagartShop_15");//Path
                    String path24 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_TagartShop_24");//Path
                    String path36 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_TagartShop_36");//Path
                    String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_TagartShop_48");//Path

                    //Check if directory exist
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                    }
                    string imageName = cnt + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";

                 //   string imgPath = Path.Combine(path, imageName);      
                                                                           

                    string imgPath15 = Path.Combine(path15, imageName);    
                    string imgPath24 = Path.Combine(path24, imageName);    

                    string imgPath36 = Path.Combine(path36, imageName);    

                    string imgPath48 = Path.Combine(path48, imageName);
                    if (cnt==1)
                    {
                        allimagename = allimagename + imageName;

                    }else
                    {
                        allimagename = allimagename +"," +imageName;
                    }
                
                    //set the image path
                    string imgPath = Path.Combine(path, imageName);

                    byte[] imageBytes = Convert.FromBase64String(idata.Image64);

                    File.WriteAllBytes(imgPath, imageBytes);

                    MemoryStream ms = new MemoryStream(imageBytes);

                    await Thumbnail.Crop(150, 150, ms, imgPath15);
                    await Thumbnail.Crop(240, 240, ms, imgPath24);
                    await Thumbnail.Crop(360, 360, ms, imgPath36);
                    await Thumbnail.Crop(480, 480, ms, imgPath48);
                    
                }
                model.Images = allimagename;
                         

                TagartShop_repository tshop = new TagartShop_repository();
               
                bool result = tshop.Insert_Update_TagartShop(model);
                if (result == true)
                {

                    response.Message = "success";
                    response.data = "";
                    response.HttpStatusCode = (int)HttpStatusCode.OK;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));


                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Error Occured.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
            }
            else
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "No values in request.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }

        }

    }
}
