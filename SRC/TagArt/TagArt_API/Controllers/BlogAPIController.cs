﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TagArt_API.Utility;
using TagArt_Business.Model;

using TagArt_Business.Repository;

namespace TagArt_API.Controllers
{
    public class BlogAPIController : ApiController
    {

        [Route("api/BlogAPI/GetBlogList/{index}")]
        [HttpGet]
        public IHttpActionResult GetBlogList(int index)
        {
            List<BlogModel> Blogmodel = new List<BlogModel>();
            Blog_repository blogobj = new Blog_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                Blogmodel = blogobj.GetAllBlogList(index);
                response.data = Blogmodel;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception)
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        [Route("api/BlogAPI/GetBlogById/BId/userId")]
        [HttpGet]
        public IHttpActionResult GetBlogById(int BId, int userId)
        {
            BlogModel Blogmodel = new BlogModel();
            Blog_repository blogobj = new Blog_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                Blogmodel = blogobj.GetBlogByID(BId, userId);
                response.data = Blogmodel;
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception)
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        [Route("api/BlogAPI/AddUpdateBlogComment/")]
        [HttpPost]
        public IHttpActionResult AddUpdateBlogComment(BlogcommentModel model)
        {

            Blogcomment_repository blogobj = new Blogcomment_repository();
            JsonResponse response = new JsonResponse();
            if (model != null)
            {
                try
                {
                    if (model.bc_BlogId == null || model.bc_BlogId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "please select blog.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                    }
                    if (model.bc_UserId == null || model.bc_UserId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "User not found.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));


                    }
                    if (model.bc_masterId == null)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "Master id  found.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));


                    }
                    if (model.bc_text.Trim() == null || model.bc_text.Trim() == "")
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "Please enter comment";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));


                    }
                    int result = blogobj.Insert_Delete_BlogComment(model);
                    if (result > 0)
                    {
                        if (model.bc_Id == 0)
                        {
                            response.data = result;
                            response.Message = "success";
                            response.HttpStatusCode = (int)HttpStatusCode.OK;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                        }
                        else
                        {
                            response.data = "";
                            response.Message = "success";
                            response.HttpStatusCode = (int)HttpStatusCode.OK;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                        }

                    }
                    else
                    {
                        response.data = "";
                        response.Message = "Error Occured.";
                        response.HttpStatusCode = (int)HttpStatusCode.OK;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));

                    }

                }
                catch (Exception)
                {

                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "Error Occured.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

                }
            }
            else
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "No values in request.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        [Route("api/BlogAPI/GetBlogComment/{blogID}/{UserId}")]
        [HttpGet]
        public IHttpActionResult GetBlogComment(int blogID, int UserId)
        {
            Blogcomment_repository blogobj = new Blogcomment_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = new DataSet();
                List<BlogcommentViewModel> model_List = new List<BlogcommentViewModel>();
                BlogcommentViewModel model1 = new BlogcommentViewModel();
                model1.BlogcommentReply = new List<BlogcommentReplyViewModel>();
                ds = blogobj.GetAllBlogCommentList(blogID, UserId, 0, 1);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BlogcommentViewModel model = new BlogcommentViewModel();
                    model.bc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]);
                    model.bc_BlogId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_BlogId"]);
                    model.bc_text = ds.Tables[0].Rows[i]["bc_text"].ToString();
                    model.bc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_UserId"]);
                    model.bc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_masterId"]);
                    model.bc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["bc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.BlogcommentReply = new List<BlogcommentReplyViewModel>();
                    List<BlogcommentReplyViewModel> childlist = new List<BlogcommentReplyViewModel>();

                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]) == (Convert.ToInt32(ds.Tables[1].Rows[j]["bc_masterId"])))
                        {
                            BlogcommentReplyViewModel bb = new BlogcommentReplyViewModel();
                            bb.bc_Id = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_Id"]);
                            bb.bc_BlogId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_BlogId"]);
                            bb.bc_text = ds.Tables[1].Rows[j]["bc_text"].ToString();
                            bb.bc_UserId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_UserId"]);
                            bb.bc_masterId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_masterId"]);
                            bb.bc_CreatedDate = Convert.ToDateTime(ds.Tables[1].Rows[j]["bc_CreatedDate"]);
                            bb.usr_FullName = ds.Tables[1].Rows[j]["usr_FullName"].ToString();
                            bb.usr_Image = ds.Tables[1].Rows[j]["usr_Image"].ToString();
                            bb.LikeComment = ds.Tables[1].Rows[j]["LikeComment"].ToString();
                            bb.more = ds.Tables[1].Rows[j]["more"].ToString();
                            childlist.Add(bb);

                        }

                    }
                    model.BlogcommentReply = childlist;
                    model_List.Add(model);
                }
                response.data = model_List;
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

        [Route("api/BlogAPI/GetBlogsubComment/{blogID}/{BlogCommtID}/{UserId}")]
        [HttpGet]
        public IHttpActionResult GetBlogsubComment(int blogID, int BlogCommtID, int UserId)
        {
            Blogcomment_repository blogobj = new Blogcomment_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = new DataSet();
                List<BlogcommentViewModel> model_List = new List<BlogcommentViewModel>();
                BlogcommentViewModel model1 = new BlogcommentViewModel();
                model1.BlogcommentReply = new List<BlogcommentReplyViewModel>();
                ds = blogobj.GetAllBlogCommentList(blogID, UserId, BlogCommtID, 2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BlogcommentViewModel model = new BlogcommentViewModel();
                    model.bc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]);
                    model.bc_BlogId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_BlogId"]);
                    model.bc_text = ds.Tables[0].Rows[i]["bc_text"].ToString();
                    model.bc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_UserId"]);
                    model.bc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_masterId"]);
                    model.bc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["bc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.more = ds.Tables[0].Rows[i]["more"].ToString();
                    model_List.Add(model);
                }
                response.data = model_List;
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));

            }
            catch (Exception ex)
            {

                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "Error Occured.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));

            }
        }

        [Route("api/BlogAPI/BlogCommentLike/")]
        [HttpPost]
        public IHttpActionResult BlogCommentLike(BlogcommentLikeModel model)
        {

            JsonResponse response = new JsonResponse();
            if (model != null)
            {
                if (model.bcl_bcId == null || model.bcl_bcId == 0)
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "please select blog comment.";
                    response.data = 0;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
                if (model.bcl_UserId == null || model.bcl_UserId == 0)
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "User not found.";
                    response.data = 0;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
                else
                {
                    BlogcommentLike_repository blogcommLike = new BlogcommentLike_repository();
                    int result = blogcommLike.Insert_Delete_BlogCommentLike(model);
                    if (result > 0)
                    {
                        if (model.bcl_Id != 0)
                        {
                            response.data = 0;
                            response.HttpStatusCode = (int)HttpStatusCode.OK;
                            response.Message = "success";
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                        }
                        else
                        {
                            response.data = result;
                            response.HttpStatusCode = (int)HttpStatusCode.OK;
                            response.Message = "success";
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                        }
                    }
                    else
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "Error Occured.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                }
            }
            else
            {
                response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "No values in request.";
                response.data = "";
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
            }
        }

    }
}
