﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TagArt_API.Utility;
using TagArt_Business.Model;

using TagArt_Business.Repository;

namespace TagArt_API.Controllers
{
    public class ArtistController : ApiController
    {
        ArtistProfile_repository apr = new ArtistProfile_repository();
        [HttpPost]
        public IHttpActionResult ArtistInsertUpdate(ArtistProfileModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if ((model.arts_name.Trim() == null || model.arts_name.Trim() == ""))
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Artist name can not be null.")));
                    }
                    if ((model.arts_createdBy == null || model.arts_createdBy == 0))
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("user can not be null.")));
                    }
                    if (model.arts_photo.Trim() != null)
                    {
                        String path = HttpContext.Current.Server.MapPath("~/ImageStorage/Artist"); //Path
                                                                                                   //Check if directory exist
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                        }
                        string imageName = model.arts_name.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                        //set the image path
                        string imgPath = Path.Combine(path, imageName);
                        byte[] imageBytes = Convert.FromBase64String(model.arts_photo.Trim());
                        File.WriteAllBytes(imgPath, imageBytes);
                        model.arts_photo = imageName;
                    }

                    model.arts_status = true;
                    bool result = apr.Insert_Update_ArtistProfile(model);
                    if (result == true)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("success", "")));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                    }
                }
                else
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("No values in request.", "")));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }
        [Route("api/Artist/GetAllArtistList")]
        [HttpGet]

        public IHttpActionResult GetAllArtistList()
        {
            List<ArtistProfileModel> artistModel = new List<ArtistProfileModel>();
            JsonResponse response = new JsonResponse();
            try
            {
                artistModel = apr.GetAllArtist();
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", artistModel)));
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
            }
        }


        [Route("api/Artist/GetArtistByName/{artsName}")]
        [HttpGet]
        public IHttpActionResult GetArtistByName(string artsName)
        {
            List<ArtistProfileModel> artistModel = new List<ArtistProfileModel>();
            JsonResponse response = new JsonResponse();
            try
            {
                artistModel = apr.SearchArtistByName(artsName.Trim());
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", artistModel)));
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
            }
        }
        [Route("api/Artist/GetArtistById/{artsId}")]
        [HttpGet]
        public IHttpActionResult GetArtistById(int artsId)
        {
            ArtistProfileModel artistModel = new ArtistProfileModel();
            JsonResponse response = new JsonResponse();
            try
            {
                artistModel = apr.GetArtist(artsId);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", artistModel)));
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
            }
        }
        [HttpGet]
        public IHttpActionResult DeleteArtist(int Id)
        {
            ArtistProfileModel artistModel = new ArtistProfileModel();
            JsonResponse response = new JsonResponse();
            try
            {
                bool result = apr.DeleteArtist(Id);
                if (result)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Artist deleted successfully.", "")));
                }
                else
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
            }
        }
    }
}
