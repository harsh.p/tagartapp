﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TagArt_API.Utility;
using TagArt_Business.Model;
using TagArt_API.Models;
using TagArt_Business.Repository;
using System.Threading.Tasks;
using System.Data;

namespace TagArt_API.Controllers
{
    public class ArtWorkController : ApiController
    {
        [HttpPost]
        [Route("api/ArtWork/ArtWorkInsertUpdate")]
        public async Task<IHttpActionResult> ArtWorkInsertUpdate(ArtWorkModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if (model.artw_arts_Name == null || model.artw_arts_Name == "")
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Artist can not be null.")));
                    }
                    if (model.atrw_age == null || model.atrw_age == 0)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Age can not be null.")));
                    }
                    if (model.artw_image == null || model.artw_image == "")
                    {
                    }
                    else
                    {
                        String path = HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork"); //Path

                        //String path = HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork");//Path
                        String path15 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_15");//Path
                        String path24 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_24");//Path
                        String path36 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_36");//Path
                        String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_48");//Path

                        //Check if directory exist
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                        }
                        string imageName = model.artw_createdby + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";

                        //set the image path
                        string imgPath = Path.Combine(path, imageName);

                        string imgPath15 = Path.Combine(path15, imageName); // string THimgname = "thum_" + data.artw_image;
                        string imgPath24 = Path.Combine(path24, imageName); // string THimgname = "thum_" + data.artw_image;
                        string imgPath36 = Path.Combine(path36, imageName); // string THimgname = "thum_" + data.artw_image;
                        string imgPath48 = Path.Combine(path48, imageName); // string THimgname = "thum_" + data.artw_image;

                        byte[] imageBytes = Convert.FromBase64String(model.artw_image.Trim());
                        File.WriteAllBytes(imgPath, imageBytes);
                        model.artw_image = imageName;
                        MemoryStream ms = new MemoryStream(imageBytes);

                        await Thumbnail.Crop(150, 150, ms, imgPath15);
                        await Thumbnail.Crop(240, 240, ms, imgPath24);
                        await Thumbnail.Crop(360, 360, ms, imgPath36);
                        await Thumbnail.Crop(480, 480, ms, imgPath48);
                    }
                    ArtWork_repository awpr = new ArtWork_repository();

                    if (model.artw_id == 0)
                        model.artw_status = 1;
                    bool result = awpr.Insert_Update_ArtWorkProfile(model);
                    if (result == true)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", "")));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                    }
                }
                else
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("No values in request.", "")));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [HttpPost]
        [Route("api/ArtWork/AddMultipleArtWork")]
        public async Task<IHttpActionResult> AddMultipleArtWork()
        {
            ArtWorkUploadResModel resData = new ArtWorkUploadResModel();
            resData.artw_uniqueID = System.Web.HttpContext.Current.Request.Form["artw_uniqueID"];
            try
            {
                JsonResponse response = new JsonResponse();
                bool result = false;
                if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form["userId"]))
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User id can not be null.", resData)));
                }
                if (System.Web.HttpContext.Current.Request.Files == null || System.Web.HttpContext.Current.Request.Files.Count == 0)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Artwork can not be null.", resData)));
                }
                ArtWorkModel artWorkModel = new ArtWorkModel();
                artWorkModel.artw_createdby = artWorkModel.userId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["userId"]);
                artWorkModel.atrw_age = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["drop_ArtAge"]);
                artWorkModel.artw_latitude = System.Web.HttpContext.Current.Request.Form["latitude"];
                artWorkModel.artw_longitude = System.Web.HttpContext.Current.Request.Form["longitude"];
                artWorkModel.artw_Info = System.Web.HttpContext.Current.Request.Form["artw_Info"];
                artWorkModel.artw_address = System.Web.HttpContext.Current.Request.Form["artw_address"];
                artWorkModel.artw_uniqueID = System.Web.HttpContext.Current.Request.Form["artw_uniqueID"];
                artWorkModel.artw_status = 1;
                var files = System.Web.HttpContext.Current.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    string fname = "";
                    string fFullname = "";

                    string fileExt = Path.GetExtension(files[i].FileName);
                    fFullname = artWorkModel.userId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + fileExt;
                    fname = Path.Combine(HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork/"), fFullname);
                    files[i].SaveAs(fname);

                    String path = HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork");
                    String path15 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_15");
                    String path24 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_24");
                    String path36 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_36");
                    String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_48");

                    //Check if directory exist
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);

                        //Create directory if it doesn't exist
                    }
                    string imageName = artWorkModel.userId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                    string imgPath = Path.Combine(path, imageName);
                    string imgPath15 = Path.Combine(path15, imageName);
                    string imgPath24 = Path.Combine(path24, imageName);
                    string imgPath36 = Path.Combine(path36, imageName);
                    string imgPath48 = Path.Combine(path48, imageName);

                    artWorkModel.artw_image = imageName;

                    //byte[] imageBytes = Convert.FromBase64String(artWorkModel.artw_images[i].Trim());

                    //Read Image File into Image object.
                    Image img = Image.FromFile(fname);

                    //ImageConverter Class convert Image object to Byte array.
                    byte[] imageBytes = (byte[])(new ImageConverter()).ConvertTo(img, typeof(byte[]));

                    //File.WriteAllBytes(imgPath, imageBytes);

                    MemoryStream ms = new MemoryStream(imageBytes);

                    await Thumbnail.Crop(150, 150, ms, imgPath15);
                    await Thumbnail.Crop(240, 240, ms, imgPath24);
                    await Thumbnail.Crop(360, 360, ms, imgPath36);
                    await Thumbnail.Crop(480, 480, ms, imgPath48);
                }
                ArtWork_repository awpr = new ArtWork_repository();
                if (artWorkModel.artw_id == 0)
                    artWorkModel.artw_status = 1;
                result = awpr.Insert_Update_ArtWorkProfile(artWorkModel);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Art uploaded successfully.", resData)));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error occured.", resData)));
            }
        }

        //[HttpPost]
        //[Route("api/ArtWork/AddMultipleArtWork")]
        //public async Task<IHttpActionResult> AddMultipleArtWork(ArtWorkModel model)
        //{
        //	try
        //	{
        //		JsonResponse response = new JsonResponse();
        //		bool result = false;

        // if (model != null) { if (model.artw_images == null && model.artw_images.Length == 0) {
        // return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
        // Helper.GenerateErrorJsonResponse("Image can not be null."))); } else { for (int i = 0; i <
        // model.artw_images.Length; i++) { String path =
        // HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork"); String path15 =
        // HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_15"); String path24 =
        // HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_24"); String path36 =
        // HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_36"); String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_48");

        // //Check if directory exist if (!System.IO.Directory.Exists(path)) {
        // System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist } string
        // imageName = model.userId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg"; string
        // imgPath = Path.Combine(path, imageName);

        // string imgPath15 = Path.Combine(path15, imageName); string imgPath24 =
        // Path.Combine(path24, imageName); string imgPath36 = Path.Combine(path36, imageName);
        // string imgPath48 = Path.Combine(path48, imageName);

        // byte[] imageBytes = Convert.FromBase64String(model.artw_images[i].Trim());

        // File.WriteAllBytes(imgPath, imageBytes); model.artw_image = imageName;

        // MemoryStream ms = new MemoryStream(imageBytes);

        // await Thumbnail.Crop(150, 150, ms, imgPath15); await Thumbnail.Crop(240, 240, ms,
        // imgPath24); await Thumbnail.Crop(360, 360, ms, imgPath36); await Thumbnail.Crop(480, 480,
        // ms, imgPath48); ArtWork_repository awpr = new ArtWork_repository();

        // // model.artw_status = 1; result = awpr.Insert_Update_ArtWorkProfile(model); } }

        //			if (result == true)
        //			{
        //				return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success")));
        //			}
        //			else
        //			{
        //				return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
        //			}
        //		}
        //		else
        //		{
        //			return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("No values in request.", "")));
        //		}
        //	}
        //	catch (Exception)
        //	{
        //		return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
        //	}
        //}

        [HttpPost]
        [Route("api/ArtWork/AdminArtWorkInsertUpdate")]
        public async Task<IHttpActionResult> AdminArtWorkInsertUpdate(ArtWorkModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if (model.artw_image.Trim() == null || model.artw_image.Trim() == "")
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Image can not be null.")));
                    }
                    else
                    {
                        String path = HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork");
                        String path15 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_15");
                        String path24 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_24");
                        String path36 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_36");
                        String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_48");

                        //Check if directory exist
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                        }
                        string imageName = model.artw_createdby + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                        string imgPath = Path.Combine(path, imageName);

                        string imgPath15 = Path.Combine(path15, imageName);
                        string imgPath24 = Path.Combine(path24, imageName);
                        string imgPath36 = Path.Combine(path36, imageName);
                        string imgPath48 = Path.Combine(path48, imageName);

                        byte[] imageBytes = Convert.FromBase64String(model.artw_image.Trim());

                        File.WriteAllBytes(imgPath, imageBytes);
                        model.artw_image = imageName;

                        MemoryStream ms = new MemoryStream(imageBytes);

                        await Thumbnail.Crop(150, 150, ms, imgPath15);
                        await Thumbnail.Crop(240, 240, ms, imgPath24);
                        await Thumbnail.Crop(360, 360, ms, imgPath36);
                        await Thumbnail.Crop(480, 480, ms, imgPath48);
                    }

                    ArtWork_repository awpr = new ArtWork_repository();
                    if (model.artw_id == 0)
                        model.artw_status = 1;
                    bool result = awpr.Insert_Update_ArtWorkProfile(model);
                    if (result == true)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success")));
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                    }
                }
                else
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("No values in request.", "")));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [HttpGet]
        [Route("api/ArtWork/ArtWorkUpdate")]
        public async Task<IHttpActionResult> ArtWorkUpdate()
        {
            try
            {
                JsonResponse response = new JsonResponse();
                ArtWork_repository awpr = new ArtWork_repository();
                List<ArtWorkModel> aw = new List<ArtWorkModel>();
                aw = awpr.GetAllArtWork("27.45214", "72.4511", 1);
                foreach (var data in aw)
                {
                    String path = HttpContext.Current.Server.MapPath("~/ImageStorage/ArtWork");//Path
                    String path15 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_15");//Path
                    String path24 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_24");//Path
                    String path36 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_36");//Path
                    String path48 = HttpContext.Current.Server.MapPath("~/ImageStorage/thum_ArtWork_48");//Path

                    string imgPath = Path.Combine(path, data.artw_image);
                    string imgPath15 = Path.Combine(path15, data.artw_image);
                    string imgPath24 = Path.Combine(path24, data.artw_image);
                    string imgPath36 = Path.Combine(path36, data.artw_image);
                    string imgPath48 = Path.Combine(path48, data.artw_image);
                    using (Image image = Image.FromFile(imgPath))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            byte[] imageBytes = m.ToArray();

                            // Convert byte[] to Base64 String

                            MemoryStream ms = new MemoryStream(imageBytes);
                            await Thumbnail.Crop(150, 150, ms, imgPath15);
                            await Thumbnail.Crop(240, 240, ms, imgPath24);
                            await Thumbnail.Crop(360, 360, ms, imgPath36);
                            await Thumbnail.Crop(480, 480, ms, imgPath48);
                        }
                    }
                }
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateSuccessJsonResponse("Success")));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/GetAllArtWorkList/{latitude}/{longitude}/{User_Id}")]
        [HttpGet]
        public IHttpActionResult GetAllArtWorkList(string latitude, string longitude, int User_Id)
        {
            ArtWork_repository awpr = new ArtWork_repository();
            List<ArtWorkModel> arWorkModel = new List<ArtWorkModel>();
            JsonResponse response = new JsonResponse();
            List<BlogModel> Blogmodel = new List<BlogModel>();
            BlogDetail_repository obj_1 = new BlogDetail_repository();
            Blog_repository blogobj = new Blog_repository();

            try
            {
                ArtworkDetailModel artmodel = new ArtworkDetailModel();
                artmodel.ArtWork = awpr.GetAllArtWork(latitude, longitude, User_Id);
                artmodel.Blog_model = blogobj.GetAllBlogList(0);
                artmodel.BlogDetail_model = obj_1.GetAllBlogDetailList();
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", artmodel)));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        // [Route("api/ArtWork/GetArtWorkByName/{latitude}/{longitude}/{artname}")]
        [HttpGet]
        public IHttpActionResult GetArtWorkByName(string latitude, string longitude, string artwname, int UserId)
        {
            ArtWork_repository awpr = new ArtWork_repository();
            List<ArtWorkModel> arWorkModel = new List<ArtWorkModel>();
            JsonResponse response = new JsonResponse();
            try
            {
                arWorkModel = awpr.SearchArtWorkByName(latitude.Trim(), longitude.Trim(), artwname.Trim(), UserId);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", arWorkModel)));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/GetArtWorkByID/{latitude}/{longitude}/{aId}/{User_Id}")]
        [HttpGet]
        public IHttpActionResult GetArtWorkByID(string latitude, string longitude, int aId, int User_Id)
        {
            ArtWork_repository awpr = new ArtWork_repository();
            ArtWorkModel arWorkModel = new ArtWorkModel();
            JsonResponse response = new JsonResponse();
            try
            {
                arWorkModel = awpr.GetArtWork(aId, User_Id, latitude.Trim(), longitude.Trim());
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", arWorkModel)));
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/GetArtWorkByArtist/{ArtistName}/{index}/{User_Id}")]
        [HttpGet]
        public IHttpActionResult GetArtWorkByArtist(string latitude, string longitude, string ArtistName, int index, int User_Id)
        {
            ArtWork_repository awpr = new ArtWork_repository();
            List<ArtWorkModel> arWorkModel = new List<ArtWorkModel>();

            JsonResponse response = new JsonResponse();
            try
            {
                arWorkModel = awpr.GetArtWorkByArtist(ArtistName.Trim(), index, User_Id, latitude.Trim(), longitude.Trim());
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", arWorkModel)));
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/DeleteArtWork/{Id}")]
        [HttpGet]
        public IHttpActionResult DeleteArtWork(int Id)
        {
            ArtWork_repository awpr = new ArtWork_repository();
            ArtWorkModel arWorkModel = new ArtWorkModel();

            JsonResponse response = new JsonResponse();
            try
            {
                bool result = awpr.DeleteArtWork(Id);
                if (result)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Art Work deleted successfully.")));
                }
                else
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/ArtworkBookMark")]
        [HttpPost]
        public IHttpActionResult ArtworkBookMark(ArtBookMarkModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if (model.ab_artwId == null || model.ab_artwId == 0)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Please select ArtWork.", 0)));
                    }
                    if (model.ab_UserId == null || model.ab_UserId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "User not found.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                    else
                    {
                        ArtBookMark_repository artBookmark = new ArtBookMark_repository();

                        int result = artBookmark.Insert_Delete_ArtBookMark(model);
                        if (result > 0)
                        {
                            if (model.ab_id != 0)
                            {
                                response.data = 0;
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                response.Message = "success";
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                            else
                            {
                                response.data = result;
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                response.Message = "success";
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                        }
                        else
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "Error Occured.";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }
                    }
                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "No values in request.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/ArtworkVisited")]
        [HttpPost]
        public IHttpActionResult ArtworkVisited(ArtVisitedModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if (model.av_artwId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "please select ArtWork.";
                        response.data = "";
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                    if (model.av_UserId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "User not found.";
                        response.data = "";
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                    else
                    {
                        ArtVisited_repository artVisited = new ArtVisited_repository();
                        ArtWorkModel artWork = new ArtWorkModel();
                        DataSet ds = new DataSet();

                        ds = artVisited.Insert_Delete_ArtVisited(model);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            artWork.artw_id = Convert.ToInt32(ds.Tables[0].Rows[0]["artw_id"]?.ToString() ?? "0");
                            artWork.artw_Name = ds.Tables[0].Rows[0]["artw_Name"]?.ToString() ?? String.Empty;
                            artWork.artw_image = ds.Tables[0].Rows[0]["artw_image"]?.ToString() ?? String.Empty;
                            var st = ds.Tables[0].Rows[0]["artw_arts_id"];
                            artWork.artw_arts_Name = ds.Tables[0].Rows[0]["artw_arts_Name"]?.ToString() ?? String.Empty;
                            artWork.artw_Info = ds.Tables[0].Rows[0]["artw_Info"]?.ToString() ?? String.Empty;
                            artWork.artw_uniqueID = ds.Tables[0].Rows[0]["artw_uniqueID"]?.ToString() ?? String.Empty;
                            artWork.artw_size = ds.Tables[0].Rows[0]["artw_size"]?.ToString() ?? String.Empty;
                            artWork.atrw_age = Convert.ToInt32(ds.Tables[0].Rows[0]["atrw_age"]?.ToString() ?? "0");
                            artWork.artw_latitude = ds.Tables[0].Rows[0]["artw_latitude"]?.ToString() ?? String.Empty;
                            artWork.artw_longitude = ds.Tables[0].Rows[0]["artw_longitude"]?.ToString() ?? String.Empty;
                            artWork.artw_address = ds.Tables[0].Rows[0]["artw_address"]?.ToString() ?? String.Empty;
                            artWork.artw_status = Convert.ToInt32(ds.Tables[0].Rows[0]["artw_status"]?.ToString() ?? "0");
                            artWork.artw_statusNotes = ds.Tables[0].Rows[0]["artw_statusNotes"]?.ToString() ?? String.Empty;
                            artWork.artw_createdby = Convert.ToInt32(ds.Tables[0].Rows[0]["artw_createdby"]?.ToString() ?? "0");
                            artWork.Visited = ds.Tables[0].Rows[0]["Visited"]?.ToString() ?? String.Empty;
                            artWork.BookMark = ds.Tables[0].Rows[0]["BookMark"]?.ToString() ?? String.Empty;

                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("success", artWork)));
                        }
                        else
                        {
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error occured.")));
                        }
                    }
                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "No values in request.";
                    response.data = 0;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/ArtLike")]
        [HttpPost]
        public IHttpActionResult ArtLike(ArtLikeModel model)
        {
            try
            {
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    if (model.al_ArtId == null || model.al_ArtId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "please select ArtWork.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                    if (model.al_UserId == null || model.al_UserId == 0)
                    {
                        response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "User not found.";
                        response.data = 0;
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                    }
                    else
                    {
                        ArtLike_repository artLike = new ArtLike_repository();

                        int result = artLike.Insert_Delete_ArtLike(model);
                        if (result > 0)
                        {
                            if (model.al_Id != 0)
                            {
                                response.data = 0;
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                response.Message = "success";
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                            else
                            {
                                response.data = result;
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                response.Message = "success";
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                        }
                        else
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "Error Occured.";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }
                    }
                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "No values in request.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/AddUpdateArtComment/")]
        [HttpPost]
        public IHttpActionResult AddUpdateArtComment(ArtCommentModel model)
        {
            try
            {
                ArtComment_repository artcommobj = new ArtComment_repository();
                JsonResponse response = new JsonResponse();
                if (model != null)
                {
                    try
                    {
                        if (model.arc_artwId == null || model.arc_artwId == 0)
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "please select art.";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }
                        if (model.arc_UserId == null || model.arc_UserId == 0)
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "User not found.";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }
                        if (model.arc_masterId == null)
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "Master id  found.";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }
                        if (model.arc_Desc.Trim() == null || model.arc_Desc.Trim() == "")
                        {
                            response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "Please enter comment";
                            response.data = 0;
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                        }

                        int result = artcommobj.Insert_Delete_ArtComment(model);
                        if (result > 0)
                        {
                            if (model.arc_Id == 0)
                            {
                                response.data = result;
                                response.Message = "success";
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                            else
                            {
                                response.data = "";
                                response.Message = "success";
                                response.HttpStatusCode = (int)HttpStatusCode.OK;
                                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
                            }
                        }
                        else
                        {
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                        }
                    }
                    catch (Exception)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                    }
                }
                else
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "No values in request.";
                    response.data = "";
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
            }
            catch (Exception)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/GetArtComment/{ArtID}/{UserId}")]
        [HttpGet]
        public IHttpActionResult GetArtComment(int ArtID, int UserId)
        {
            ArtComment_repository artobj = new ArtComment_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = new DataSet();
                List<ArtCommentViewModel> model_List = new List<ArtCommentViewModel>();
                ArtCommentViewModel model1 = new ArtCommentViewModel();
                model1.ArtcommentReply = new List<ArtCommentReplyViewModel>();
                ds = artobj.GetAllArtCommentList(ArtID, UserId, 0, 1);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArtCommentViewModel model = new ArtCommentViewModel();
                    model.arc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]);
                    model.arc_artwId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_artwId"]);
                    model.arc_Desc = ds.Tables[0].Rows[i]["arc_Desc"].ToString().Trim();
                    model.arc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_UserId"]);
                    model.arc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_masterId"]);
                    model.arc_status = Convert.ToBoolean(ds.Tables[0].Rows[i]["arc_status"]);

                    model.arc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["arc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.ArtcommentReply = new List<ArtCommentReplyViewModel>();
                    List<ArtCommentReplyViewModel> childlist = new List<ArtCommentReplyViewModel>();

                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]) == (Convert.ToInt32(ds.Tables[1].Rows[j]["arc_masterId"])))
                        {
                            ArtCommentReplyViewModel ac = new ArtCommentReplyViewModel();
                            ac.arc_Id = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_Id"]);
                            ac.arc_artwId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_artwId"]);
                            ac.arc_Desc = ds.Tables[1].Rows[j]["arc_Desc"].ToString().Trim();
                            ac.arc_UserId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_UserId"]);
                            ac.arc_masterId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_masterId"]);
                            ac.arc_status = Convert.ToBoolean(ds.Tables[1].Rows[j]["arc_status"]);
                            ac.arc_CreatedDate = Convert.ToDateTime(ds.Tables[1].Rows[j]["arc_CreatedDate"]);
                            ac.usr_FullName = ds.Tables[1].Rows[j]["usr_FullName"].ToString();
                            ac.usr_Image = ds.Tables[1].Rows[j]["usr_Image"].ToString();
                            ac.LikeComment = ds.Tables[1].Rows[j]["LikeComment"].ToString();
                            ac.more = ds.Tables[1].Rows[j]["more"].ToString();
                            childlist.Add(ac);
                        }
                    }
                    model.ArtcommentReply = childlist;
                    model_List.Add(model);
                }
                response.data = model_List;
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/GetArtsubComment/{ArtID}/{ArtCommtID}/{UserId}")]
        [HttpGet]
        public IHttpActionResult GetArtsubComment(int ArtID, int ArtCommtID, int UserId)
        {
            ArtComment_repository artCommobj = new ArtComment_repository();
            JsonResponse response = new JsonResponse();
            try
            {
                DataSet ds = new DataSet();
                List<ArtCommentViewModel> model_List = new List<ArtCommentViewModel>();
                ArtCommentViewModel model1 = new ArtCommentViewModel();
                model1.ArtcommentReply = new List<ArtCommentReplyViewModel>();
                ds = artCommobj.GetAllArtCommentList(ArtID, UserId, ArtCommtID, 2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArtCommentViewModel model = new ArtCommentViewModel();
                    model.arc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]);
                    model.arc_artwId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_artwId"]);
                    model.arc_Desc = ds.Tables[0].Rows[i]["arc_Desc"].ToString().Trim();
                    model.arc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_UserId"]);
                    model.arc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_masterId"]);
                    model.arc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["arc_CreatedDate"]);
                    model.arc_status = Convert.ToBoolean(ds.Tables[0].Rows[i]["arc_status"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.more = ds.Tables[0].Rows[i]["more"].ToString();
                    model_List.Add(model);
                }
                response.data = model_List;
                response.Message = "success";
                response.HttpStatusCode = (int)HttpStatusCode.OK;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, response));
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
            }
        }

        [Route("api/ArtWork/ArtCommentLike/")]
        [HttpPost]
        public IHttpActionResult ArtCommentLike(ArtCommentLikeModel model)
        {
            JsonResponse response = new JsonResponse();
            if (model != null)
            {
                if (model.arcl_arc_Id == null || model.arcl_arc_Id == 0)
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "please select art comment.";
                    response.data = 0;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
                if (model.arcl_UserId == null || model.arcl_UserId == 0)
                {
                    response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "User not found.";
                    response.data = 0;
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
                }
                else
                {
                    ArtCommentLike_repository ArtcommLike = new ArtCommentLike_repository();
                    int result = ArtcommLike.Insert_Delete_ArtCommentLike(model);
                    if (result > 0)
                    {
                        if (model.arcl_Id != 0)
                        {
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", 0)));
                        }
                        else
                        {
                            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Success", result)));
                        }
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("Error Occured.", "")));
                    }
                }
            }
            else
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateErrorJsonResponse("No values in request.")));
            }
        }
    }
}