﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TagArt_API.Utility;
using TagArt_Business.Business.Helpers;
using TagArt_Business.Model;
using System.Data;
using Newtonsoft.Json;
using TagArt_Business.Repository;

namespace TagArt_API.Controllers
{
	public class AccountAPIController : ApiController
	{
		private UserList_repository usl = new UserList_repository();

		[Route("api/AccountAPI/Login/{UserEmail}/{UserPassword}/{LoginType}")]
		[HttpPost]
		public IHttpActionResult Login(string UserEmail, string UserPassword, int LoginType)
		{
			JsonResponse response = new JsonResponse();
			try
			{
				if (string.IsNullOrEmpty(UserEmail))
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("UserEmail or User name can not be null")));
				}
				if (string.IsNullOrEmpty(UserPassword))
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Password can not be null")));
				}
				if (UserEmail != null && (UserPassword != null || LoginType != 0))
				{
					if (LoginType == 3)
					{
						UserListModel user = usl.GetLogin(UserEmail, UserPassword, LoginType);
						if (user != null)
						{
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("You have successfully logged in", user)));
						}
						else
						{
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Invalid email/username or password")));
						}
					}
					else
					{
						//1-facebook,2-google, 3-email
						if (LoginType == 1)
						{
							response = Helper.GenerateErrorJsonResponse("You have registered with facebook.Please try login with facebook");
						}
						if (LoginType == 2)
						{
							response = Helper.GenerateErrorJsonResponse("You have registered with google.Please try login with google");
						}
						if (LoginType != 1 && LoginType != 2 && LoginType != 3 && LoginType != 4)
						{
							response = Helper.GenerateErrorJsonResponse("Invalid email/username or password");
						}
						response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, response));
					}
				}
			}
			catch (Exception ex)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured")));
			}
			return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Invalid Username Or Password")));
		}

		[HttpPost]
		[Route("api/AccountAPI/Register")]
		public IHttpActionResult Register(UserListModel model)
		{
			JsonResponse response = new JsonResponse();
			if (model != null)
			{
                if (model.usr_LoginType < 1 || model.usr_LoginType > 4) //usr_LoginType =3 -email and usr_LoginType =1 -facebook ,usr_LoginType =2 -google,usr_LoginType =4 apple
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Invalid loginType.")));
                }
                if (model.usr_LoginType == 3)
                {
                    if (string.IsNullOrEmpty(model.usr_FullName))
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Full Name can not be null.")));
                    }
                    if (string.IsNullOrEmpty(model.usr_UserName))
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User Name can not be null.")));
                    }
                    if (model.usr_UserEmail == null || model.usr_UserEmail.Trim() == "")
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Email can not be null.")));
                    }
                    if (string.IsNullOrEmpty(model.usr_UserPassword) && model.usr_LoginType == 3)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Password can not be null.")));
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(model.usr_FullName))
                    {
                        model.usr_FullName = string.Empty;
                    }
                    if (string.IsNullOrEmpty(model.usr_UserName))
                    {
                        model.usr_UserName = string.Empty;
                    }
                    if (model.usr_UserEmail == null || model.usr_UserEmail.Trim() == "")
                    {
                        model.usr_UserEmail=String.Empty;
                    }
                    if (string.IsNullOrEmpty(model.usr_UserPassword))
                    {
                        model.usr_UserPassword=String.Empty;
                    }
                }

				if (!string.IsNullOrEmpty(model.usr_Image))
				{
					String path = HttpContext.Current.Server.MapPath("~/ImageStorage/UserProfile"); //Path

					//Check if directory exist
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
					}
					string imageName = model.usr_UserName.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";

					//set the image path
					string imgPath = Path.Combine(path, imageName);

					byte[] imageBytes = Convert.FromBase64String(model.usr_Image.Trim());

					File.WriteAllBytes(imgPath, imageBytes);
					model.usr_Image = imageName;
				}

				UserListModel um = new UserListModel();
				List<UserListModel> um_list = new List<UserListModel>();
				DataSet ds = new DataSet();

				ds = usl.InserUpdateUser(model);

				if (ds.Tables[0].Rows.Count > 0)
				{
					um.usr_UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["usr_UserId"]);
					um.usr_UserName = ds.Tables[0].Rows[0]["usr_UserName"].ToString();
					um.usr_FullName = ds.Tables[0].Rows[0]["usr_FullName"].ToString();
					um.usr_UserEmail = ds.Tables[0].Rows[0]["usr_UserEmail"].ToString();
					um.usr_UserPassword = ds.Tables[0].Rows[0]["usr_UserPassword"].ToString();
					um.usr_LoginType = Convert.ToInt32(ds.Tables[0].Rows[0]["usr_LoginType"]);
					um.usr_UserPassword = ds.Tables[0].Rows[0]["usr_UserPassword"].ToString();
					um.usr_Status = Convert.ToInt32(ds.Tables[0].Rows[0]["usr_Status"]);
					um.usr_Token = ds.Tables[0].Rows[0]["usr_Token"].ToString();
					um.usr_Image = ds.Tables[0].Rows[0]["usr_Image"].ToString();
					um.usr_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["usr_CreatedDate"]);
                    um.Country = ds.Tables[0].Rows[0]["Country"].ToString(); ;

                    um.usr_Role = Convert.ToInt32(ds.Tables[0].Rows[0]["usr_Role"]);

					string rtype = ds.Tables[0].Rows[0]["rtype"].ToString();

					if (rtype == "old")
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("User already register.", um)));
					}
					else if (rtype == "new")
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("You have successfully register.", um)));
					}
					else if (rtype == "username")
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Please enter unique username.")));
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("This email address already registered with us.")));
					}
				}
				else
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error occured.")));
				}
			}
			else
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error occured.")));
			}
		}

		[HttpPost]
        //[Route("api/AccountAPI/RegiFindAccountster/{usr_UserEmail}")]
        public IHttpActionResult RegiFindAccountster(string usr_UserEmail)
		{
			JsonResponse response = new JsonResponse();
			if (string.IsNullOrEmpty(usr_UserEmail))
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Email can not be null.")));
			}
			else
			{
				try
				{
					UserListModel ulm = usl.FindAccount(usr_UserEmail.Trim());
					if (ulm != null)
					{
						string EmailAddrres = ulm.usr_UserEmail.ToString();
						string UserId = ulm.usr_UserId.ToString();
						string UserName = ulm.usr_FullName.ToString();
						if (EmailAddrres != "" && UserId != "" && SendEmailHelper.IsValidEmailAddress(EmailAddrres) == true)
						{
							if (ulm.usr_LoginType == 3)
							{
								string OTP = RandomString(6);
								string MessageString = SendEmailHelper.ReadFile(ConfigurationManager.AppSettings["EmailPath"] + "\\" + "ForgotPassword.html");
								MessageString = MessageString.ToString().Replace("{OTP}", OTP).Replace("{Name}", UserName);

								bool data = false;
								Task.Run(async () =>
								{
									data = SendEmailHelper.AccountSecuritySendMailMessage(EmailAddrres, "Forgot Password", MessageString, "");
								}).Wait();
								if (data == true)
								{
									return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("We have sent you reset password otp on your email account.", OTP)));
								}
								else
								{
									return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured")));
								}
							}
							else
							{
								return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Can't reset the password because your account is facebook/google.")));
							}
						}
						else
						{
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("invalid email ")));
						}
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Email address not found")));
					}
				}
				catch (Exception ex)
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.", ex)));
				}
			}
		}

		[HttpGet]
		[Route("api/AccountAPI/UserNameVerify/{UserName}")]
		public IHttpActionResult UserNameVerify(string UserName)
		{
			JsonResponse response = new JsonResponse();
			if (UserName?.Trim() == null || UserName?.Trim() == "")
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User Name can not be null.")));
			}
			else
			{
				try
				{
					UserListModel ulm = new UserListModel();
					bool result = usl.UserNameVerify(UserName.Trim());
					if (result == true)
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User name already exist.")));
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("User name available.", "")));
					}
				}
				catch (Exception ex)
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
		}

		[Route("api/AccountAPI/PasswordReset/{UserEmail}/{newpassword}")]
		[HttpPost]
		public IHttpActionResult PasswordReset(string UserEmail, string newpassword)
		{
			JsonResponse response = new JsonResponse();
            if (UserEmail == "" && UserEmail == "" && SendEmailHelper.IsValidEmailAddress(UserEmail) == false)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("invalid email ")));
            }
			else if (string.IsNullOrEmpty(newpassword))
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("new password can not be null.")));
			}
			else
			{
				try
				{
					UserListModel ulm = new UserListModel();
					bool result = usl.ChangePasswordOTP(UserEmail, newpassword.Trim(), 1);
					if (result == true)
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Password changed successfully", "")));
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Password not changed.")));
					}
				}
				catch (Exception ex)
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
		}

		[Route("api/AccountAPI/UserProfile/{latitude}/{longitude}/{UserId}")]
		[HttpPost]
		public IHttpActionResult UserProfile(string latitude, string longitude, int UserId)
		{
			JsonResponse response = new JsonResponse();
			if (UserId == 0)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Enter valid userid.", "")));
			}
			else if (string.IsNullOrEmpty(latitude))
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("latitude/longitude can not be null.", "")));
			}
			else
			{
				try
				{
					UserListModel ulm = new UserListModel();

					List<ArtWorkModel> Allartlist = new List<ArtWorkModel>();
					List<ArtWorkModel> artlistBookMark = new List<ArtWorkModel>();
					List<ArtWorkModel> artlistVisited = new List<ArtWorkModel>();

					UserListUserWiseModel model = new UserListUserWiseModel();
					model.UserListModel = usl.GetUserProfileDetail(UserId, 1);//user detail
					model.AllUsrArtWork = usl.GetAllArtWorkByUserId(latitude, longitude, UserId, 2); // all user art
					model.AllUsrArtWorkVisited = usl.GetAllArtWorkByUserId(latitude, longitude, UserId, 3); // only visted art
					model.AllUsrArtWorkBookMark = usl.GetAllArtWorkByUserId(latitude, longitude, UserId, 4); // only bookmark art
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Successfully.", model)));
				}
				catch (Exception ex)
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
		}

		//admin can userimage upload
		[Route("api/AccountAPI/UpdateUserProfileImage")]
		[HttpPost]
		public async Task<IHttpActionResult> UpdateUserProfileImage(UpdateUserProfileImageModel obj)
		{
			JsonResponse response = new JsonResponse();
			try
			{
				if (!string.IsNullOrEmpty(obj.usr_Image) && obj.usr_UserId > 0)
				{
					var userDetail = usl.GetUserProfileDetail(obj.usr_UserId, 1);//user detail
					String path = HttpContext.Current.Server.MapPath("~/ImageStorage/UserProfile"); //Path

					//Check if directory exist
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
					}

					string imageName = userDetail.usr_UserName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";

					//set the image path
					string imgPath = Path.Combine(path, imageName);

					byte[] imageBytes = Convert.FromBase64String(obj.usr_Image);

					File.WriteAllBytes(imgPath, imageBytes);
					userDetail.usr_Image = imageName;

					bool status1 = usl.UpdateUser(userDetail);
					if (status1 == true)
					{
						var updatedUserDetail = usl.GetUserProfileDetail(obj.usr_UserId, 1);//user detail
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Profile image updated successfully.", updatedUserDetail)));
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error occured while updating user profile image.")));
					}
				}
				else
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
			catch (Exception ex)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
			}
		}

		//admin can userimage upload
		[Route("api/AccountAPI/UserProfileImage")]
		[HttpPost]
		public async Task<IHttpActionResult> UserProfileImage(UserListModel obj)
		{
			JsonResponse response = new JsonResponse();
			try
			{
				if (!string.IsNullOrEmpty(obj.usr_Image))
				{
					String path = HttpContext.Current.Server.MapPath("~/ImageStorage/UserProfile"); //Path

					//Check if directory exist
					if (!System.IO.Directory.Exists(path))
					{
						System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
					}

					// string imageName = obj.usr_UserId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
					//set the image path
					string imgPath = Path.Combine(path, obj.usr_Token);

					byte[] imageBytes = Convert.FromBase64String(obj.usr_Image);

					File.WriteAllBytes(imgPath, imageBytes);
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Profile image updated successfully.", 1)));
				}
				else
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
			catch (Exception ex)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
			}
		}

		[Route("api/AccountAPI/UpdateUserProfile")] //mobile api
		[HttpPost]
		public IHttpActionResult UpdateUserProfile(UserListUpdateProfileModel model)
		{
			JsonResponse response = new JsonResponse();
			try
			{
				if (model != null)
				{
					if (model.usr_UserId != 0)
					{
						var userDetail = usl.GetUserProfileDetail(model.usr_UserId, 1);//user detail
						if (userDetail == null)
						{
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Invalid user id.")));
						}
						if ((!string.IsNullOrEmpty(model.usr_UserName)))
						{
							bool result = usl.UserNameVerify(model.usr_UserName.Trim());
							if (result == true)
							{
								return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User name already exist.")));
							}
							else
							{
								userDetail.usr_UserName = model.usr_UserName;
							}
						}
						if ((!string.IsNullOrEmpty(model.usr_UserEmail)))
						{
							bool result = usl.EmailVerify(model.usr_UserEmail.Trim());
							if (result == true)
							{
								return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("This email address already registered with us.")));
							}
							else
							{
								userDetail.usr_UserEmail = model.usr_UserEmail;
							}
						}
						if ((!string.IsNullOrEmpty(model.usr_UserCurrentPassword) && !string.IsNullOrEmpty(model.usr_UserNewPassword)))
						{
							if (userDetail.usr_LoginType == 3)
							{
								if (model.usr_UserNewPassword.Trim().ToLower() != model.usr_UserNewConfirmPassword.Trim().ToLower())
								{
									return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Password and confirm password did not match.")));
								}
								else
								{
									bool result = usl.VerifyPassword(model.usr_UserId, model.usr_UserCurrentPassword.Trim());
									if (result == true)
									{
										if (model.usr_UserCurrentPassword.Trim().ToLower() == model.usr_UserNewPassword.Trim().ToLower())
										{
											return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Old password and new password can not be same.")));
										}
										else
										{
											userDetail.usr_UserPassword = model.usr_UserNewPassword;
										}
									}
									else
									{
										return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Current password didn't match.")));
									}
								}
							}
							else
							{
								return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Can't reset the password because your account is facebook/google.")));
							}
						}

						//write code here for enable/disable user
						if (!string.IsNullOrEmpty(model.usr_Status))
						{
							if (model.usr_Status == "1" || model.usr_Status == "0")
							{
								userDetail.usr_Status = Convert.ToInt16(model.usr_Status);
							}
						}
						if (!string.IsNullOrEmpty(model.usr_Image))
						{
							String path = HttpContext.Current.Server.MapPath("~/ImageStorage/UserProfile"); //Path

							//Check if directory exist
							if (!System.IO.Directory.Exists(path))
							{
								System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
							}
							string imageName = model.usr_UserName.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";

							//set the image path
							string imgPath = Path.Combine(path, imageName);

							byte[] imageBytes = Convert.FromBase64String(model.usr_Image.Trim());

							File.WriteAllBytes(imgPath, imageBytes);
							userDetail.usr_Image = imageName;
						}
						bool status1 = usl.UpdateUser(userDetail);
						if (status1 == true)
						{
							var UpdatedUserDetail = usl.GetUserProfileDetail(model.usr_UserId, 1);//user detail
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Helper.GenerateSuccessJsonResponse("Profile updated successfully.", UpdatedUserDetail)));
						}
						else
						{
							return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
						}
					}
					else
					{
						return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("User id required.")));
					}
				}
				else
				{
					return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
				}
			}
			catch (Exception ex)
			{
				return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, Helper.GenerateErrorJsonResponse("Error Occured.")));
			}
		}

		private static Random random = new Random();

		public static string RandomString(int length)
		{
			const string chars = "0123456789";
			return new string(Enumerable.Repeat(chars, length)
			  .Select(s => s[random.Next(s.Length)]).ToArray());
		}
	}
}