﻿using System;
using System.Configuration;
using System.Web;
using System.Net.Mail;
using System.Text.RegularExpressions;


namespace TagArt_Business.Business.Helpers
{
    public class SendEmailHelper
	{
		/// <summary>
		/// Sends Email Message
		/// </summary>
		/// <param name="toEmail">Sender Address</param>
		/// <param name="subject">Email Subject</param>
		/// <param name="body">Email Message body</param>
		/// <param name="attachmentFullPath">File attachment path</param>
		/// <returns></returns>
		public static bool AccountSecuritySendMailMessage(string toEmail, string subject, string body, string attachmentFullPath)
		{
			try
			{
				//create the MailMessage object
				MailMessage mMailMessage = new MailMessage();
                string rs = "";
				string fromEmail = ConfigurationManager.AppSettings["AccountSecurityUserName"];
				string fromPassword = ConfigurationManager.AppSettings["AccountSecurityUserPassword"];
				//set the sender address of the mail message
				if (!string.IsNullOrEmpty(fromEmail))
				{
					mMailMessage.From = new MailAddress(fromEmail, "");
				}

				//set the recipient address of the mail message
				mMailMessage.To.Add(new MailAddress(toEmail));

				//set the subject of the mail message
				if (!string.IsNullOrEmpty(subject))
				{
					mMailMessage.Subject = subject;
				}
				else
				{
					mMailMessage.Subject = "Forgot Password";
				}

				//set the body of the mail message
				mMailMessage.Body = body;
				//set the format of the mail message body

				mMailMessage.IsBodyHtml = true;

				//set the priority
				mMailMessage.Priority = MailPriority.High;

				if (!string.IsNullOrEmpty(attachmentFullPath))
				{
					Attachment mailAttachment = new Attachment(attachmentFullPath);
					mMailMessage.Attachments.Add(mailAttachment);
				}

				//create the SmtpClient instance

				SmtpClient mSmtpClient = new SmtpClient(ConfigurationManager.AppSettings["AccountSecurityServerName"]);
				mSmtpClient.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
				mSmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]); // Gmail works on this port
             //   mSmtpClient.UseDefaultCredentials = false;
              //  mSmtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                mSmtpClient.EnableSsl = true;
                //send the mail message

                mSmtpClient.Timeout = 10000;
                //Task.Run(async () =>
                //{
                  mSmtpClient.Send(mMailMessage);
               // }).Wait();
                return true;
            }
			catch (Exception ex)
			{
				//ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
			}
		}

		public static bool CustomerServiceSendMailMessage(string toEmail, string subject, string body, string attachmentFullPath)
		{
			try
			{
				//create the MailMessage object
				MailMessage mMailMessage = new MailMessage();

				string fromEmail = ConfigurationManager.AppSettings["CustomerServiceMailAddress"];
				string fromPassword = ConfigurationManager.AppSettings["CustomerServiceMailPassword"];
				//set the sender address of the mail message
				if (!string.IsNullOrEmpty(fromEmail))
				{
					mMailMessage.From = new MailAddress(fromEmail, "VICAP Team");
				}

				//set the recipient address of the mail message
				mMailMessage.To.Add(new MailAddress(toEmail));

				//set the subject of the mail message
				if (!string.IsNullOrEmpty(subject))
				{
					mMailMessage.Subject = subject;
				}
				else
				{
					mMailMessage.Subject = "VICAP Account";
				}

				//set the body of the mail message
				mMailMessage.Body = body;
				//set the format of the mail message body

				mMailMessage.IsBodyHtml = true;

				//set the priority
				mMailMessage.Priority = MailPriority.High;

				if (!string.IsNullOrEmpty(attachmentFullPath))
				{
					Attachment mailAttachment = new Attachment(attachmentFullPath);
					mMailMessage.Attachments.Add(mailAttachment);
				}

				//create the SmtpClient instance

				SmtpClient mSmtpClient = new SmtpClient(ConfigurationManager.AppSettings["CustomerServiceServerName"]);
				mSmtpClient.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
				mSmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]); // Gmail works on this port
				mSmtpClient.EnableSsl = true;
				//send the mail message
				mSmtpClient.Send(mMailMessage);
				return true;
			}
			catch (Exception ex)
			{
				//ErrorSignal.FromCurrentContext().Raise(ex);
				return false;
			}
		}

		/// <summary>
		/// Determines whether an email address is valid.
		/// </summary>
		/// <param name="emailAddress">The email address to validate.</param>
		/// <returns>
		/// 	<c>true</c> if the email address is valid; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsValidEmailAddress(string emailAddress)
		{
			// An empty or null string is not valid
			if (String.IsNullOrEmpty(emailAddress))
			{
				return (false);
			}

			// Regular expression to match valid email address
			string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
								@"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
								@".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

			// Match the email address using a regular expression
			Regex re = new Regex(emailRegex);
			if (re.IsMatch(emailAddress))
				return (true);
			else
				return (false);
		}

		public static string ReadFile(string filename)
		{
			string MessageString = string.Empty;
			try
			{
				System.IO.StreamReader oRead;
                string ApplicationPath = HttpContext.Current.Server.MapPath(filename);
                oRead = System.IO.File.OpenText(ApplicationPath);
				MessageString = oRead.ReadToEnd();
				oRead.Close();
				oRead = null;
			}
			catch (Exception ex)
			{
				//ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return MessageString;
		}
	}
}