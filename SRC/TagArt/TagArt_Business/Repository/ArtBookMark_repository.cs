﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class ArtBookMark_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_ArtBookMark(ArtBookMarkModel obj)
        {
            try
            {
                int result = 0;
                DynamicParameters param = new DynamicParameters();
                ConnectionCheck();
                SqlCommand command = new SqlCommand("Insert_Delete_ArtBookMark_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ab_id", obj.ab_id);
                command.Parameters.AddWithValue("@ab_UserId", obj.ab_UserId);
                command.Parameters.AddWithValue("@ab_artwId", obj.ab_artwId);
                if (obj.ab_id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();

                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }

        }
        public List<ArtBookMarkModel> GetArtBookMark(int Artistid, int index)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@artw_arts_id", Artistid);
                param.Add("@index", index);
                param.Add("@Action", 4);
                return db.Query<ArtBookMarkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }


    }
}
