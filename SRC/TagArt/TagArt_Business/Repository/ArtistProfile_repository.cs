﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;
namespace TagArt_Business.Repository
{
    public class ArtistProfile_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public bool Insert_Update_ArtistProfile(ArtistProfileModel objcd)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@arts_Id", objcd.arts_Id);
                param.Add("@arts_name", objcd.arts_name.Trim());
                param.Add("@arts_photo", objcd.arts_photo.Trim());
                param.Add("@arts_info", objcd.arts_info.Trim());
                param.Add("@arts_status", objcd.arts_status);
                param.Add("@arts_createdBy", objcd.arts_createdBy);
                param.Add("@arts_ModifyBy", objcd.arts_ModifyBy);

                if (objcd.arts_Id == 0)
                { param.Add("@Action", 1); }
                else { param.Add("@Action", 2); }

                ConnectionCheck();
                con.Execute("Insert_Update_ArtistProfile_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public List<ArtistProfileModel> GetAllArtist()
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetArtistProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@Action", 1);
                return db.Query<ArtistProfileModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public List<ArtistProfileModel> SearchArtistByName(string arts_name)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetArtistProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@arts_name", arts_name);
                param.Add("@Action", 3);
                return db.Query<ArtistProfileModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public ArtistProfileModel GetArtist(int Id)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetArtistProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@arts_Id", Id);

                param.Add("@Action", 2);
                if (db.Query<ArtistProfileModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
                {
                    return db.Query<ArtistProfileModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
                }
                else { return null; }

            }
        }
        public bool DeleteArtist(int Id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(con_nstring))
                {
                    string readSp = "Insert_Update_ArtistProfile_sp";
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@arts_Id", Id);

                    param.Add("@Action", 3);
                    con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
                    con.Close();
                    return true;

                }
            }
            catch (Exception ex)
            {
                       return false;
            }
        }

    }
}
