﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class ArtVisited_repository
    {

        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public DataSet Insert_Delete_ArtVisited(ArtVisitedModel obj)
        {
            try
            {
                DataSet ds = new DataSet();

                ConnectionCheck();

                SqlCommand command = new SqlCommand("Insert_Delete_ArtVisited_sp", con);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@av_id", obj.av_id);
                command.Parameters.AddWithValue("@av_UserId", obj.av_UserId);
                command.Parameters.AddWithValue("@av_artwId", obj.av_artwId);

                if (obj.av_id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }

                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
