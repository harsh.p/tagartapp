﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public  class ArtComment_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_ArtComment(ArtCommentModel obj)
        {
            int result = 0;
            try
            {
                DynamicParameters param = new DynamicParameters();
                ConnectionCheck();
                SqlCommand command = new SqlCommand("Insert_Update_ArtComment_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@arc_Id", obj.arc_Id);
                command.Parameters.AddWithValue("@arc_artwId", obj.arc_artwId);
                command.Parameters.AddWithValue("@arc_Desc", obj.arc_Desc.Trim());
                command.Parameters.AddWithValue("@arc_UserId", obj.arc_UserId);
                command.Parameters.AddWithValue("@arc_masterId", obj.arc_masterId);
                command.Parameters.AddWithValue("@arc_status", obj.arc_status);         

                if (obj.arc_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();


                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }

        }
        
        public DataSet GetAllArtCommentList(int ArtId, int UserId, int ArtCommtID, int Action)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetArtcommentList_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                DynamicParameters param = new DynamicParameters();

                command.Parameters.AddWithValue("@arc_artwId", ArtId);
                command.Parameters.AddWithValue("@arc_Id", ArtCommtID);
                command.Parameters.AddWithValue("@arc_UserId", UserId);
                command.Parameters.AddWithValue("@Action", Action);


                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();

                return ds;
             
            }

        }


    }
}
