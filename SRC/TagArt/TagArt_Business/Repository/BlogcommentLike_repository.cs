﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class BlogcommentLike_repository
    {

        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_BlogCommentLike(BlogcommentLikeModel obj)
        {
            int result = 0;
            try
            {
                DynamicParameters param = new DynamicParameters();
                ConnectionCheck();
                SqlCommand command = new SqlCommand("Insert_Delete_BlogCommentLike_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@bcl_Id", obj.bcl_Id);
                command.Parameters.AddWithValue("@bcl_bcId", obj.bcl_bcId);
                command.Parameters.AddWithValue("@bcl_UserId", obj.bcl_UserId);
                if (obj.bcl_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();

                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }

        }

    }
}
