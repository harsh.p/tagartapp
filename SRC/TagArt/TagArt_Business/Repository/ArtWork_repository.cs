﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;
using Elmah;

namespace TagArt_Business.Repository
{
    public class ArtWork_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public bool Insert_Update_ArtWorkProfile(ArtWorkModel objcd)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();

                param.Add("@artw_id", objcd.artw_id);
                param.Add("@artw_image", objcd.artw_image != null ? objcd.artw_image.Trim() : objcd.artw_image);
                param.Add("@artw_arts_Name", objcd.artw_arts_Name != null ? objcd.artw_arts_Name.Trim() : objcd.artw_arts_Name);
                param.Add("@artw_Info", objcd.artw_Info != null ? objcd.artw_Info.Trim() : objcd.artw_Info);
                param.Add("@artw_size", objcd.artw_size != null ? objcd.artw_size.Trim() : objcd.artw_size);
                param.Add("@atrw_age", objcd.atrw_age);
                param.Add("@artw_latitude", objcd.artw_latitude != null ? objcd.artw_latitude.Trim() : objcd.artw_latitude);
                param.Add("@artw_longitude", objcd.artw_longitude != null ? objcd.artw_longitude.Trim() : objcd.artw_longitude);
                param.Add("@artw_address", objcd.artw_address != null ? objcd.artw_address.Trim() : objcd.artw_address);
                param.Add("@artw_status", objcd.artw_status);
                param.Add("@artw_uniqueID", objcd.artw_uniqueID);
                param.Add("@artw_statusNotes", objcd.artw_statusNotes != null ? objcd.artw_statusNotes.Trim() : objcd.artw_statusNotes);
                param.Add("@artw_createdby", objcd.artw_createdby);
                param.Add("@artw_modifyBy", objcd.artw_modifyBy);

                if (objcd.artw_id == 0)
                { param.Add("@Action", 1); }
                else { param.Add("@Action", 2); }

                ConnectionCheck();
                con.Execute("Insert_Update_ArtWorkProfile_tbl_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }


        public List<ArtWorkModel> AdminDashboardArtWork(int index, string artist, string Age, string Status, string StartDate, string EndDate, string UserList)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@index", index);
                param.Add("@artw_arts_Name", artist);
                param.Add("@atrw_age", Age);
                param.Add("@artw_status", Status);
                param.Add("@UserList", UserList);
                param.Add("@StartDate", StartDate);
                param.Add("@EndDate", EndDate);
                param.Add("@Action", 7);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public List<ArtWorkModel> AdminDashboardFilter(int index, string Option1, string Option2, string Action)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@index", index);
                param.Add("@artw_arts_Name", Option1);
                param.Add("@atrw_age", Option2);
                param.Add("@Action", Action);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<arts_List_Model> GetAllArtistName()
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@Action", 6);
                return db.Query<arts_List_Model>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public List<ArtWorkModel> GetAllArtWork(string artw_latitude, string artw_longitude, int User_Id)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@artw_latitude_current", artw_latitude);
                param.Add("@artw_longitude_current", artw_longitude);
                param.Add("@User_Id", User_Id);

                param.Add("@Action", 1);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public List<ArtWorkModel> SearchArtWorkByName(string artw_latitude, string artw_longitude, string artw_arts_Name, int User_Id)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@artw_latitude_current", artw_latitude);
                param.Add("@artw_longitude_current", artw_longitude);
                param.Add("@artw_arts_Name", artw_arts_Name);
                param.Add("@User_Id", User_Id);
                param.Add("@Action", 3);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public ArtWorkModel GetArtWork(int Id, int User_Id, string latitude, string longitude)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(con_nstring))
                {
                    string readSp = "GetAllArtWorkProfile_sp";
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@artw_id", Id);
                    param.Add("@User_Id", User_Id);
                    param.Add("@artw_latitude_current", latitude);
                    param.Add("@artw_longitude_current", longitude);


                    param.Add("@Action", 2);

                    if (db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
                    {
                        return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
                    }
                    else { return null; }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ArtWorkModel> GetArtWorkByArtist(string ArtistName, int index, int User_Id, string latitude, string longitude)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();

                param.Add("@artw_arts_Name", ArtistName);
                param.Add("@index", index);
                param.Add("@User_Id", User_Id);
                param.Add("@artw_latitude_current", latitude);
                param.Add("@artw_longitude_current", longitude);
                param.Add("@artw_arts_Name", ArtistName);

                param.Add("@Action", 4);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<ArtWorkModel> AdminGetArtWorkByArtist(int Artistid, int index)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetAllArtWorkProfile_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@artw_arts_id", Artistid);
                param.Add("@index", index);
                param.Add("@Action", 6);
                return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public bool DeleteArtWork(int Id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(con_nstring))
                {
                    string readSp = "Insert_Update_ArtWorkProfile_tbl_sp";
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@artw_id", Id);
                    param.Add("@Action", 3);
                    con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
                    con.Close();
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update_ArtWorkAction(int artw_id, int artw_status)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@artw_id", artw_id);
                param.Add("@artw_status", artw_status);
                param.Add("@Action", 1);
                ConnectionCheck();
                con.Execute("ArtStatusUpdate_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Location_Model> GetLocationList()
        {
            try
            {
                using (IDbConnection db = new SqlConnection(con_nstring))
                {
                    string readSp = "GetLocation_sp";
                    DynamicParameters param = new DynamicParameters();
                    return db.Query<Location_Model>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
    }
}
