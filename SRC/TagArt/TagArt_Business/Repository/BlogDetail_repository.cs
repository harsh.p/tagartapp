﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;
namespace TagArt_Business.Repository
{
    public class BlogDetail_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public bool Insert_Update_BlogDetail(BlogDetailModel objcd)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@bd_id", objcd.bd_id);
                param.Add("@bd_BlogId", objcd.bd_BlogId);
                param.Add("@bd_type", objcd.bd_type);
                param.Add("@bd_Value", objcd.bd_Value);
                param.Add("@bd_order", objcd.bd_order);
                param.Add("@bd_createdBy", objcd.bd_createdBy);
                param.Add("@bd_ModifyBy", objcd.bd_ModifyBy);
             
             
                if (objcd.bd_id == 0)
                { param.Add("@Action", 1); }
                else { param.Add("@Action", 2); }

                ConnectionCheck();
                con.Execute("Insert_Update_BlogDetail_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }



        public bool Update_BlogDetailOrder(int bd_id, int bd_order)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@bd_id", bd_id);
               
                param.Add("@bd_order", bd_order);
                param.Add("@Action", 4);
                         

                ConnectionCheck();
                con.Execute("Insert_Update_BlogDetail_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }

        public List<BlogDetailModel> GetBlogDetailList(int BDID)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "BlogDetail_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@bd_BlogId", BDID);
                param.Add("@Action", 1);
                return db.Query<BlogDetailModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public List<BlogDetailModel> GetAllBlogDetailList()
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "BlogDetail_sp";
                DynamicParameters param = new DynamicParameters();
              
                param.Add("@Action", 3);
                return db.Query<BlogDetailModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        
        public bool DeleteBlogDetailt(int bd_id)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(con_nstring))
                {
                    string readSp = "Insert_Update_BlogDetail_sp";
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@bd_id", bd_id);

                    param.Add("@Action", 3);
                    con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
                    con.Close();
                    return true;

                }
            }
            catch (Exception ex)
            {


                return false;
            }
        }

    }
}
