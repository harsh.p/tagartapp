﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;
using System.Collections.Generic;
using System.Linq;

namespace TagArt_Business.Repository
{
    public class TagartShop_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        public bool Insert_Update_TagartShop(TagartShopModel objtshop)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ts_Id", objtshop.ts_Id);
                param.Add("@ts_Title", objtshop.ts_Title!=null? objtshop.ts_Title.Trim(): objtshop.ts_Title);
                param.Add("@ts_size", objtshop.ts_size!=null?objtshop.ts_size.Trim(): objtshop.ts_size);
                param.Add("@ts_Price", objtshop.ts_Price!=null? objtshop.ts_Price.Trim(): objtshop.ts_Price);
                param.Add("@ts_description", objtshop.ts_description!=null? objtshop.ts_description.Trim(): objtshop.ts_description);
                param.Add("@ts_Email", objtshop.ts_Email!=null? objtshop.ts_Email.Trim(): objtshop.ts_Email);
                param.Add("@ts_phone", objtshop.ts_phone!=null?objtshop.ts_phone.Trim(): objtshop.ts_phone);
                param.Add("@ts_type", objtshop.ts_type);
                param.Add("@ts_Letitude", objtshop.ts_Letitude!=null? objtshop.ts_Letitude.Trim():objtshop.ts_Letitude);
                param.Add("@ts_Logitude", objtshop.ts_Logitude!=null? objtshop.ts_Logitude.Trim(): objtshop.ts_Logitude);
                param.Add("@ts_Address", objtshop.ts_Address!=null? objtshop.ts_Address.Trim(): objtshop.ts_Address);
                param.Add("@ts_UserId", objtshop.ts_UserId);
                param.Add("@ts_Status", objtshop.ts_Status);
                param.Add("@Images", objtshop.Images!=null? objtshop.Images.Trim(): objtshop.Images);
              

                if (objtshop.ts_Id == 0)
                { param.Add("@Action", 1); }
                else { param.Add("@Action", 2); }

                ConnectionCheck();
                con.Execute("Insert_Update_Delete_TagartShop_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }


        public bool Delete_TagartShop(int ID)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@TsId", ID);
                param.Add("@Action", 3);
                ConnectionCheck();
                con.Execute("Insert_Update_TagartShop_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
               
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
            
        }
        public List<TagartShopModel> GetAllTagartShop(int index, string date_To, string date_From, int status,int price,  int UserId)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetTagartShop_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@index", index);
                param.Add("@date_From", date_From);
                param.Add("@date_To", date_To);
                param.Add("@filter_status", status);
                param.Add("@User_Id", UserId);
                param.Add("@Action", 3);
                return db.Query<TagartShopModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public TagartShopModel GetTagartShopByID(int Id)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetTagartShop_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@TsId", Id);
               
                param.Add("@Action", 1);
                if (db.Query<TagartShopModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
                {
                    return db.Query<TagartShopModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
                }
                else { return null; }

            }
        }

    }
}
