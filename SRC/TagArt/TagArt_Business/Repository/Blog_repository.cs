﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class Blog_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public List<BlogModel> GetAllBlogList(int index)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetBlog_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@index", index);
                param.Add("@Action", 1);
                return db.Query<BlogModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<BlogModel> GetAllBlogWeb(int index, string date_To, string date_From, int status,int UserId)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetBlog_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@index", index);
                param.Add("@date_From", date_From);
                param.Add("@date_To", date_To);
                param.Add("@filter_status", status);
                param.Add("@User_Id", UserId);
                param.Add("@Action", 3);
                return db.Query<BlogModel>(readSp, param, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public BlogModel GetBlogByID(int bId,int UserId)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                string readSp = "GetBlog_sp";
                DynamicParameters param = new DynamicParameters();
                param.Add("@bId", bId);
                param.Add("@User_Id", UserId);
                param.Add("@Action", 2);
                if (db.Query<BlogModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
                {
                    return db.Query<BlogModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
                }
                else { return null; }

            }
        }

        public bool Insert_Update_BlogTitle(BlogModel objcd)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@bId", objcd.bId);
                param.Add("@bTitle", objcd.bTitle);
                param.Add("@bImage", objcd.bImage);
                param.Add("@bStatus", objcd.bStatus);
                param.Add("@bDecs", objcd.bDescription);
                param.Add("@bCreatedBy", objcd.bCreatedBy);
                param.Add("@bModifyBy", objcd.bModifyBy);
                          

                if (objcd.bId == 0)
                { param.Add("@Action", 1); }
                else { param.Add("@Action", 2); }

                ConnectionCheck();
                con.Execute("Insert_Update_Blog_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }

        public bool Delete_Blog(BlogModel objcd)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@bId", objcd.bId);
                param.Add("@Action", 3);
                ConnectionCheck();
                con.Execute("Insert_Update_Blog_sp", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }

        }


    }
}
