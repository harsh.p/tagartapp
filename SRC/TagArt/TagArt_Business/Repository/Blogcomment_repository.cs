﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class Blogcomment_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_BlogComment(BlogcommentModel obj)
        {
            int result = 0;
            try
            {

                DynamicParameters param = new DynamicParameters();


                ConnectionCheck();


                SqlCommand command = new SqlCommand("Insert_Update_Blogcomment_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@bc_Id", obj.bc_Id);
                command.Parameters.AddWithValue("@bc_BlogId", obj.bc_BlogId);
                command.Parameters.AddWithValue("@bc_text", obj.bc_text.Trim());
                command.Parameters.AddWithValue("@bc_UserId", obj.bc_UserId);
                command.Parameters.AddWithValue("@bc_masterId", obj.bc_masterId);

                if (obj.bc_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();


                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }

        }
        public DataSet GetAllBlogCommentList(int BlogId, int UserId, int BlogCommtID, int Action)
        {
            using (IDbConnection db = new SqlConnection(con_nstring))
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand("GetBlogcommentList_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                DynamicParameters param = new DynamicParameters();

                command.Parameters.AddWithValue("@bc_BlogId", BlogId);
                command.Parameters.AddWithValue("@bc_id", BlogCommtID);
                command.Parameters.AddWithValue("@bc_UserId", UserId);
                command.Parameters.AddWithValue("@Action", Action);
                using (SqlDataAdapter sda = new SqlDataAdapter(command))
                {
                    sda.Fill(ds);
                }
                con.Close();

                return ds;

            }

        }
    }
}
