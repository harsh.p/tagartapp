﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elmah;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
	public class UserList_repository
	{
		private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
		private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);

		public void ConnectionCheck()
		{
			if (con.State == ConnectionState.Closed)
			{
				con.Open();
			}
		}

		public int CheckUserEmail(string Email, int UserId)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "checkUserEmail";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserEmail", Email);
					param.Add("@usr_UserId", UserId);
					return db.Query<int>(readSp, param, commandType: CommandType.StoredProcedure).FirstOrDefault();
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}

		public UserListModel FindAccount(string Email)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "FindAccount_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@Action", "1");
					param.Add("@usr_UserEmail", Email);

					if (db.Query<UserListModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
					{
						return db.Query<UserListModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
					}
					else { return null; }
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}

		public UserListModel GetLogin(string Email, string Password, int LoginType)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserlogin_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserEmail", Email);
					param.Add("@usr_UserPassword", Password);
					param.Add("@usr_LoginType", LoginType);//1-facebook,2-google, 3-email
					if (db.Query<UserListModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
					{
						return db.Query<UserListModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
					}
					else { return null; }
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public DataSet InserUpdateUser(UserListModel objcd)
		{
			try
			{
				ConnectionCheck();

				DataSet ds = new DataSet();

				SqlCommand command = new SqlCommand("Insert_Update_UserList_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				DynamicParameters param = new DynamicParameters();
				command.Parameters.AddWithValue("@usr_UserId", 0);
				command.Parameters.AddWithValue("@usr_FullName", objcd.usr_FullName);
				command.Parameters.AddWithValue("@usr_UserName", objcd.usr_UserName);
				command.Parameters.AddWithValue("@usr_UserEmail", objcd.usr_UserEmail);
				command.Parameters.AddWithValue("@usr_UserPassword", objcd.usr_UserPassword);
				command.Parameters.AddWithValue("@usr_LoginType", objcd.usr_LoginType); //1-facebook,2-google, 3-email
				command.Parameters.AddWithValue("@usr_Status", 1);
				command.Parameters.AddWithValue("@usr_Token", objcd.usr_Token);
				command.Parameters.AddWithValue("@usr_Image", objcd.usr_Image);
				command.Parameters.AddWithValue("@Country", objcd.Country);
				command.Parameters.AddWithValue("@usr_Role", 3);

				using (SqlDataAdapter sda = new SqlDataAdapter(command))
				{
					sda.Fill(ds);
				}
				con.Close();

				return ds;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public DataSet InserUpdateUserFromAdmin(UserListModel objcd)
		{
			try
			{
				ConnectionCheck();

				DataSet ds = new DataSet();

				SqlCommand command = new SqlCommand("Insert_Update_UserList_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				DynamicParameters param = new DynamicParameters();
				command.Parameters.AddWithValue("@usr_UserId", 0);
				command.Parameters.AddWithValue("@usr_FullName", objcd.usr_FullName);
				command.Parameters.AddWithValue("@usr_UserName", objcd.usr_UserName);
				command.Parameters.AddWithValue("@usr_UserEmail", objcd.usr_UserEmail);
				command.Parameters.AddWithValue("@usr_UserPassword", objcd.usr_UserPassword);
				command.Parameters.AddWithValue("@usr_LoginType", objcd.usr_LoginType); //1-facebook,2-google, 3-email
				command.Parameters.AddWithValue("@usr_Status", 1);
				command.Parameters.AddWithValue("@usr_Token", objcd.usr_Token);
				command.Parameters.AddWithValue("@usr_Image", objcd.usr_Image);
				command.Parameters.AddWithValue("@Country", objcd.Country);
				command.Parameters.AddWithValue("@usr_Role", objcd.usr_Role);

				using (SqlDataAdapter sda = new SqlDataAdapter(command))
				{
					sda.Fill(ds);
				}
				con.Close();

				return ds;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateUser(UserListModel objcd)
		{
			try
			{
				//ConnectionCheck();
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "UpdateUserList_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserId", objcd.usr_UserId);
					param.Add("@usr_FullName", objcd.usr_FullName);
					param.Add("@usr_UserName", objcd.usr_UserName);
					param.Add("@usr_UserEmail", objcd.usr_UserEmail);
					param.Add("@usr_UserPassword", objcd.usr_UserPassword);
					param.Add("@usr_Image", objcd.usr_Image);
					param.Add("@usr_Status", objcd.usr_Status);
					param.Add("@Country", objcd.Country);
					param.Add("@usr_Role", objcd.usr_Role);
					con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
					con.Close();
					return true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateUserStaus(UserListModel objcd)
		{
			try
			{
				//ConnectionCheck();

				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "UpdateUserStatusUserList_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserId", objcd.usr_UserId);
					param.Add("@usr_Status", objcd.usr_Status);
					con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
					con.Close();
					return true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool DeleteUser(UserListModel objcd)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "DeleteUserFromUserList_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserId", objcd.usr_UserId);
					con.Execute(readSp, param, commandType: CommandType.StoredProcedure);
					con.Close();
					return true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public string RecoverPassword(string Key)
		{
			string result = "0";
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("FindAccount_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@Action", "2");
				command.Parameters.AddWithValue("@usr_UserEmail", Key);
				SqlDataReader rdr = command.ExecuteReader();
				if (rdr.Read())
				{
					result = rdr["StatusResult"].ToString();
				}
				rdr.Close();
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public string ChangePassword(int Userid, string Password, int Action)
		{
			string result = "fail";
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("ChangePassword_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@usr_UserId", Userid);
				command.Parameters.AddWithValue("@usr_UserPassword", Password);
				command.Parameters.AddWithValue("@Action", Action);

				int rowsAffected = command.ExecuteNonQuery();
				if (rowsAffected == 1)
				{
					result = "success";
				}
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public bool ChangePasswordOTP(string UserEmail, string Password, int Action)
		{
			bool result = false;
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("ChangePassword_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@usr_UserPassword", Password);
                command.Parameters.AddWithValue("@usr_UserEmail", UserEmail);
                command.Parameters.AddWithValue("@Action", Action);

				int rowsAffected = command.ExecuteNonQuery();
				if (rowsAffected == 1)
				{
					result = true; ;
				}
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public bool UserNameVerify(string UserName)
		{
			bool result = false;
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("FindAccount_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@usr_UserName", UserName);
				command.Parameters.AddWithValue("@Action", '3');

				int rowsAffected = (int)command.ExecuteScalar();
				if (rowsAffected == 1)
				{
					result = true; ;
				}
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public bool EmailVerify(string Email)
		{
			bool result = false;
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("FindAEmail_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@usr_UserEmail", Email);

				int rowsAffected = (int)command.ExecuteScalar();
				if (rowsAffected > 0)
				{
					result = true;
				}
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public bool VerifyPassword(int UserId, string CurrentPassword)
		{
			bool result = false;
			try
			{
				ConnectionCheck();
				SqlCommand command = new SqlCommand("VerifyPassword_sp", con);
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("@usr_UserId", UserId);
				command.Parameters.AddWithValue("@usr_UserPassword", CurrentPassword);

				int rowsAffected = (int)command.ExecuteScalar();
				if (rowsAffected > 0)
				{
					result = true;
				}
				con.Close();
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
			}
			return result;
		}

		public List<UserListModel_Admin> GetUserList(string ID)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserList_sp";
					DynamicParameters param = new DynamicParameters();
					return db.Query<UserListModel_Admin>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList();
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}

		public List<UserDetailsListModel_Admin> GetUserDetailsList(string ID)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserProfileFromUserList_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserId", ID);
					return db.Query<UserDetailsListModel_Admin>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList();
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}

		public List<ArtWorkModel> GetAllArtWorkByUserId(string latitude, string longitude, int User_Id, int Action)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserProfile_sp";
					DynamicParameters param = new DynamicParameters();
					param.Add("@usr_UserId", User_Id);
					param.Add("@latitude", latitude);
					param.Add("@longitude", longitude);
					param.Add("@Action", Action);
					return db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public UserListModel GetUserProfileDetail(int UserID, int Action)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserProfile_sp";
					DynamicParameters param = new DynamicParameters();

					param.Add("@usr_UserId", UserID);
					param.Add("@Action", Action);

					if (db.Query<ArtWorkModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList().Count > 0)
					{
						return db.Query<UserListModel>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList()?.First();
					}
					else { return null; }
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}

		public List<User_Art_Upload> GetUserArtUpload(string UserID)
		{
			try
			{
				using (IDbConnection db = new SqlConnection(con_nstring))
				{
					string readSp = "GetUserArtUpload_sp";
					DynamicParameters param = new DynamicParameters();
					return db.Query<User_Art_Upload>(readSp, param, commandType: CommandType.StoredProcedure)?.ToList();
				}
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw;
			}
		}
	}
}