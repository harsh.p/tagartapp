﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class ArtCommentLike_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_ArtCommentLike(ArtCommentLikeModel obj)
        {
            int result = 0;
            try
            {
                DynamicParameters param = new DynamicParameters();
                ConnectionCheck();
                SqlCommand command = new SqlCommand("Insert_Delete_ArtCommentLike_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@arcl_Id", obj.arcl_Id);
                command.Parameters.AddWithValue("@arcl_arc_Id", obj.arcl_arc_Id);
                command.Parameters.AddWithValue("@arcl_UserId", obj.arcl_UserId);
                if (obj.arcl_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();

                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }

        }
    }
}
