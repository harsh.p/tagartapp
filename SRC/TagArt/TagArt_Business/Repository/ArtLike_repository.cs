﻿using System;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using TagArt_Business.Model;

namespace TagArt_Business.Repository
{
    public class ArtLike_repository
    {
        private string con_nstring = System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString;
        private SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TagArt_DB"].ConnectionString);
        public void ConnectionCheck()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public int Insert_Delete_ArtLike(ArtLikeModel obj)
        {
            int result = 0;
            try
            {
           
                DynamicParameters param = new DynamicParameters();
               ConnectionCheck();
               SqlCommand command = new SqlCommand("Insert_Delete_ArtLike_sp", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@al_Id", obj.al_Id);
                command.Parameters.AddWithValue("@al_ArtId", obj.al_ArtId);
                command.Parameters.AddWithValue("@al_UserId", obj.al_UserId);
                if (obj.al_Id == 0)
                {
                    command.Parameters.AddWithValue("@Action", 1);
                }
                else
                {
                    command.Parameters.AddWithValue("@Action", 2);
                }
                result = (int)command.ExecuteScalar();

                con.Close();
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }

        }
    }
}
