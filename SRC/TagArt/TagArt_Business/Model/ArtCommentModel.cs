﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtCommentModel
    {

         public int arc_Id { get; set; }
        public int arc_artwId { get; set; }
        public string arc_Desc { get; set; }
        public int arc_UserId { get; set; }
        public int arc_masterId { get; set; }
        public bool arc_status { get; set; }
        public DateTime arc_CreatedDate { get; set; }
        public DateTime arc_ModifyDate { get; set; }

    }
    public class ArtCommentViewModel
    {
        public int arc_Id { get; set; }
        public int arc_artwId { get; set; }
        public string arc_Desc { get; set; }
        public int arc_UserId { get; set; }
        public int arc_masterId { get; set; }
        public bool arc_status { get; set; }
        public DateTime arc_CreatedDate { get; set; }
        public DateTime arc_ModifyDate { get; set; }
        public string usr_FullName { get; set; }
        public string usr_Image { get; set; }
        public string LikeComment { get; set; }
        public string more { get; set; }
        public List<ArtCommentReplyViewModel>ArtcommentReply { get; set; }


    }
    public class ArtCommentReplyViewModel
    {
        public int arc_Id { get; set; }
        public int arc_artwId { get; set; }
        public string arc_Desc { get; set; }
        public int arc_UserId { get; set; }
        public int arc_masterId { get; set; }
        public bool arc_status { get; set; }
        public DateTime arc_CreatedDate { get; set; }
        public DateTime arc_ModifyDate { get; set; }
        public string usr_FullName { get; set; }
        public string usr_Image { get; set; }
        public string LikeComment { get; set; }
        public string more { get; set; }


    }
}
