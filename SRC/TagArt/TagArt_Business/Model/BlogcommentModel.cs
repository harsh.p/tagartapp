﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class BlogcommentModel
    {
        public int bc_Id { get; set; }
        public int bc_BlogId { get; set; }
        public string bc_text { get; set; }
        public int bc_UserId { get; set; }
        public int bc_masterId { get; set; }
        public DateTime bc_CreatedDate { get; set; }
        public DateTime bc_ModifyDate { get; set; }

    }
    public class BlogcommentViewModel
    {
        public int bc_Id { get; set; }
        public int bc_BlogId { get; set; }
        public string bc_text { get; set; }
        public int bc_UserId { get; set; }
        public int bc_masterId { get; set; }
        public DateTime bc_CreatedDate { get; set; }
        public DateTime bc_ModifyDate { get; set; }
        public string usr_FullName { get; set; }
        public string usr_Image { get; set; }
        public string LikeComment { get; set; }
        public string more { get; set; }
        public List<BlogcommentReplyViewModel> BlogcommentReply { get; set; }


    }
    public class BlogcommentReplyViewModel
    {
        public int bc_Id { get; set; }
        public int bc_BlogId { get; set; }
        public string bc_text { get; set; }
        public int bc_UserId { get; set; }
        public int bc_masterId { get; set; }
        public DateTime bc_CreatedDate { get; set; }
        public DateTime bc_ModifyDate { get; set; }
        public string usr_FullName { get; set; }
        public string usr_Image { get; set; }
        public string LikeComment { get; set; }
        public string more { get; set; }


    }
}
