﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtLikeModel
    {

        public int al_Id { get; set; }
        public int al_ArtId { get; set; }
        public int al_UserId { get; set; }
        public DateTime al_CreatedDate { get; set; }

    }
}
