﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
	public class UserListUpdateProfileModel
	{
		public int usr_UserId { get; set; }

		public string usr_UserName { get; set; }

		public string usr_UserEmail { get; set; }

		public string usr_UserCurrentPassword { get; set; }

		public string usr_UserNewPassword { get; set; }

		public string usr_UserNewConfirmPassword { get; set; }

		public string usr_Status { get; set; }

		public string usr_Image { get; set; }
	}

	public class UpdateUserProfileImageModel
	{
		public string usr_Image { get; set; }

		public int usr_UserId { get; set; }
	}

	public class UserListModel
	{
		public int usr_UserId { get; set; }

		public string usr_FullName { get; set; }

		public string usr_UserName { get; set; }

		public string usr_UserEmail { get; set; }

		public string usr_UserPassword { get; set; }

		public int usr_LoginType { get; set; }

		public int usr_Status { get; set; }

		public string usr_Token { get; set; }

		public string usr_Image { get; set; }

		public DateTime usr_CreatedDate { get; set; }

		public DateTime usr_UpdatedDate { get; set; }

		public bool usr_IsLoggedIn { get; set; }

		public int usr_Role { get; set; }

		public string Country { get; set; }

		public int usr_Ass { get; set; }

		public int usr_upload { get; set; }

		public List<UserListModel> UserList { get; set; }
	}

	public class UserListModel_Admin
	{
		public int usr_UserId { get; set; }

		public string usr_FullName { get; set; }

		public string usr_UserName { get; set; }

		public string usr_UserEmail { get; set; }

		public string usr_UserPassword { get; set; }

		public int usr_LoginType { get; set; }

		public bool usr_Status { get; set; }

		public string usr_Token { get; set; }

		public string usr_Image { get; set; }

		public DateTime usr_CreatedDate { get; set; }

		public DateTime usr_UpdatedDate { get; set; }

		public bool usr_IsLoggedIn { get; set; }

		public int usr_Role { get; set; }

		public int usr_Ass { get; set; }

		public int usr_upload { get; set; }

		public string SType { get; set; }

		public string LType { get; set; }

		public string RType { get; set; }

		public int SR { get; set; }

		public string CreatedDate { get; set; }

		public int Art { get; set; }

		public int ArtApproved { get; set; }

		public int ArtPending { get; set; }

		public int ArtIssue { get; set; }

		public int ArtRejected { get; set; }

		public int ArtDeleted { get; set; }

		public int Comment { get; set; }

		public int Visited { get; set; }

		public int BookMark { get; set; }

		public int artLike { get; set; }

		public string Country { get; set; }

		public string ActivityDate { get; set; }

		public string ActivityDays { get; set; }

		public int ArtFlagged { get; set; }

		public int CommentsFlagged { get; set; }

		public string Flag { get; set; }

		public List<UserListModel_Admin> UserList { get; set; }
	}

	public class User_Art_Upload
	{
		public int SR { get; set; }

		public int usr_UserId { get; set; }

		public string usr_FullName { get; set; }

		public string artw_arts_Name { get; set; }

		public string artw_image { get; set; }

		public string Comments { get; set; }

		public string Visited { get; set; }

		public string BookMark { get; set; }

		public string artLike { get; set; }

		public string ArtStatus { get; set; }

		public string Country { get; set; }

		public string CreatedDate { get; set; }

		public List<User_Art_Upload> User_Art_UploadList { get; set; }
	}

	public class UserDetailsListModel_Admin
	{
		public int usr_UserId { get; set; }

		public string Totals { get; set; }

		public string Uploads { get; set; }

		public int TotalofStatus { get; set; }

		public string Comments { get; set; }

		public string Visited { get; set; }

		public string BookMark { get; set; }

		public string artLike { get; set; }

		public List<UserDetailsListModel_Admin> UserDetailsList { get; set; }
	}

	public class UserListUserWiseModel
	{
		public UserListModel UserListModel { get; set; }

		public List<ArtWorkModel> AllUsrArtWork { get; set; }

		public List<ArtWorkModel> AllUsrArtWorkBookMark { get; set; }

		public List<ArtWorkModel> AllUsrArtWorkVisited { get; set; }
	}
}