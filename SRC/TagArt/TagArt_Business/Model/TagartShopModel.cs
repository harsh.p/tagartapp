﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class TagartShopModel
    {
        
        public int ts_Id { get; set; }
        public List<TagartShopImageModel> TagartShopImage { get; set; }
        public List<TagArtshopImageList>  ImageList { get; set; }
        public string Images { get; set; }
        public string ts_Title { get; set; }
        public string ts_size { get; set; }
        public string ts_Price { get; set; }
        public string ts_description { get; set; }
        public string ts_Email { get; set; }
        public string ts_phone { get; set; }
        public int ts_type { get; set; }
        public string ts_Letitude { get; set; }
        public string ts_Logitude { get; set; }
        public string ts_Address { get; set; }
        public int ts_UserId { get; set; }
        public string  UserName { get; set; }
        public int ts_Status { get; set; }
        public int ts_TotalArt { get; set; }
        public int LeftArt { get; set; }
        public DateTime ts_createdDate { get; set; }
        public DateTime ts_ModifyDate { get; set; }

    }

    public class TagartShopImageModel
    {
        public int tsi_Id { get; set; }
        public int tsi_tshopId { get; set; }
        public int tsi_Image { get; set; }


    }
    public class TagArtshopImageList
    {
        public string Image64 { get; set; }           

    }
}
