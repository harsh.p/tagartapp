﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
	public class ArtWorkUploadResModel
	{
		public string artw_uniqueID { get; set; }
	}

	public class ArtWorkModel
	{
		public int artw_id { get; set; }

		public string artw_Name { get; set; }

		public string artw_image { get; set; }

		public string[] artw_images { get; set; }

		// public string artw_Img_thumbnail { get; set; }
		public int artw_arts_id { get; set; }

		public string artw_arts_Name { get; set; }

		public string artw_Info { get; set; }

        public string artw_uniqueID { get; set; }

        public string artw_size { get; set; }

		public int atrw_age { get; set; }

		public string artw_latitude { get; set; }

		public string artw_longitude { get; set; }

		public string artw_address { get; set; }

		public string km { get; set; }

		public int artw_status { get; set; }

		public string artw_statusNotes { get; set; }

		public int userId { get; set; }

		public int artw_createdby { get; set; }

		public DateTime artw_createdDate { get; set; }

		public int artw_modifyBy { get; set; }

		public DateTime artw_modifyDate { get; set; }

		public string arts_name { get; set; }

		public string arts_photo { get; set; }

		public string BookMark { get; set; }

		public string Visited { get; set; }

		public string ArtLike { get; set; }

		public string TotalArtLike { get; set; }

		public string TotalArtComment { get; set; }

		public string UserName { get; set; }
	}

	public class arts_List_Model
	{
		public string artw_arts_Name { get; set; }
	}
}