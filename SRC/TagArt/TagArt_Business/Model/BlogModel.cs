﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TagArt_Business.Model
{
    public class BlogModel
    {

        public int bId { get; set; }
        public string bTitle { get; set; }
        public string bImage { get; set; }
        public bool bStatus { get; set; }
        [AllowHtml]
        public string bDescription { get; set; }

        public int bCreatedBy { get; set; }
        public DateTime bCreatedDate { get; set; }
        public int bModifyBy { get; set; }
        public DateTime bModifyDate { get; set; }

        public string BookMark { get; set; }
        public string BlogLike { get; set; }
        public string TotalBlogComment { get; set; }
        public string TotalBlogLike { get; set; }


    }

    public class BlogimageURL
    {
        public string Url { get; set; }
    }
}
