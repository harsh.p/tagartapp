﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtBookMarkModel
    {
      
        public int ab_id { get; set; }
        public int ab_UserId { get; set; }
        public int ab_artwId { get; set; }
        public DateTime ab_CreatedDate { get; set; }
    }
}
