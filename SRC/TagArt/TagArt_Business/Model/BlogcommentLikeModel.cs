﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class BlogcommentLikeModel
    {
        public int bcl_Id { get; set; }
        public int bcl_bcId { get; set; }
        public int bcl_UserId { get; set; }
        public DateTime bcl_CreatedDate { get; set; }

    }
}
