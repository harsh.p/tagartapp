﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtistProfileModel
    {

        public int arts_Id { get; set; }
    
        public string arts_name { get; set; }
        public string arts_photo { get; set; }
      
        public string arts_info { get; set; }
        public bool arts_status { get; set; }
        public int arts_createdBy { get; set; }
        public DateTime arts_creadtedDate { get; set; }
        public int arts_ModifyBy { get; set; }
        public DateTime arts_ModifyDate { get; set; }
        public string Upload { get; set; }
    }
}
