﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class BlogDetailModel
    {
       
        public int bd_id { get; set; }
        public int bd_BlogId { get; set; }
        public int bd_type { get; set; }
        public string bd_Value { get; set; }
        public int bd_order { get; set; }
        public int bd_createdBy { get; set; }
        public DateTime bd_createdDate { get; set; }
        public int bd_ModifyBy { get; set; }
        public DateTime bd_ModifyDate { get; set; }

    }
}
