﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtCommentLikeModel
    {

        public int arcl_Id { get; set; }
        public int arcl_arc_Id { get; set; }
        public int arcl_UserId { get; set; }
        public DateTime arcl_CreatedDate { get; set; }

    }
}
