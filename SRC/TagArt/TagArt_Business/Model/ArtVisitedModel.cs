﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class ArtVisitedModel
    {  
        public int av_id { get; set; }
        public int av_UserId { get; set; }
        public int av_artwId { get; set; }
        public DateTime av_createdDate { get; set; }

    }
}
