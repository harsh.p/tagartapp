﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagArt_Business.Model
{
    public class Location_Model
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string Address { get; set; }
        public int ID { get; set; }
    }
}
