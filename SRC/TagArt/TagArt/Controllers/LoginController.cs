﻿using System;
using System.Web.Mvc;
using TagArt_Business.Repository;
using TagArt_Business.Model;
using System.Configuration;
using TagArt_Business.Business.Helpers;
using TagArt_Business.Helpers;
using System.Threading.Tasks;
using Elmah;

namespace TagArt.Controllers
{
	public class LoginController : Controller
	{
		// GET: Login

		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public ActionResult UserLogin(string UserID, string UserPassword)
		{
			try
			{
				UserList_repository ulr = new UserList_repository();
				UserListModel ulm = new UserListModel();
				string adminId = System.Configuration.ConfigurationManager.AppSettings["AdminUserId"];
				string adminPassword = System.Configuration.ConfigurationManager.AppSettings["AdminUserPassword"];

				ulm = ulr.GetLogin(UserID, UserPassword, 3);//1-facebook,2-google, 3-email

				if (ulm != null)
				{
					if (ulm.usr_Role != 3)
					{
						Session["usr_Image"] = ulm.usr_Image;
						Session["usr_UserId"] = ulm.usr_UserId;
						Session["usr_UserEmail"] = ulm.usr_UserEmail;
						Session["usr_FullName"] = ulm.usr_FullName;
						Session["usr_UserName"] = ulm.usr_UserName;
						Session["usr_Role"] = ulm.usr_Role;

						return Json(new { status = "success" });
					}
					else
					{
						return Json(new { status = "fail" });
					}
				}
				else
				{
					return Json(new { status = "fail" });
				}
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return Json(new { status = ex.Message });
			}
		}

		public ActionResult LogOut()
		{
			Session["usr_Image"] = null;
			Session["usr_UserId"] = null;
			Session["usr_UserEmail"] = null;
			Session["usr_FullName"] = null;
			Session["usr_UserName"] = null;
			return View("Login");
		}

		[HttpPost]
		public ActionResult FindAccount(string Email)
		{
			string Responce = string.Empty;
			try
			{
				UserList_repository ulr = new UserList_repository();
				UserListModel ulm = new UserListModel();
				ulm = ulr.FindAccount(Email);
				if (ulm != null)
				{
					string EmailAddrres = ulm.usr_UserEmail.ToString();
					string UserId = ulm.usr_UserId.ToString();
					string UserName = ulm.usr_FullName.ToString();
					if (EmailAddrres != "" && UserId != "" && SendEmailHelper.IsValidEmailAddress(EmailAddrres) == true)
					{
						string MessageString = SendEmailHelper.ReadFile(ConfigurationManager.AppSettings["EmailPath"] + "\\" + "ForgotPassword.html");
						MessageString = MessageString.ToString().Replace("{Url}", ConfigurationManager.AppSettings["URLpath"] + "Login\\ChangePassword?session=" + Common_Function.Base64Encode(ulm.usr_UserEmail.ToString())).Replace("{Name}", UserName);
						bool data = false;
						Task.Run(async () =>
						{
							data = SendEmailHelper.AccountSecuritySendMailMessage(EmailAddrres, "Forgot Password", MessageString, "");
						}).Wait();
						if (data == true)
						{
							Responce = "success";
						}
						else { Responce = "fail"; }
					}
					else
					{
						Responce = "fail";
					}
				}
				else
				{
					Responce = "NotFound";
				}
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return Json(new { data_Email = ex.Message });
			}
			return Json(new { data_Email = Responce });
		}

		public ActionResult ChangePassword(string session)
		{
			try
			{
				UserList_repository ulr = new UserList_repository();
				string result = ulr.RecoverPassword(Common_Function.Base64Decode(session));

				ViewBag.UserId = result;
				return View();
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return null;
			}
		}

		public ActionResult PasswordChange(int User_Id, string User_Password)
		{
			try
			{
				UserList_repository ulr = new UserList_repository();
				string result = ulr.ChangePassword(User_Id, User_Password, 1);

				return Json(new { status = result });
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return null;
			}
		}
	}
}