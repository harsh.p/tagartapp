﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using TagArt.Models;

namespace TagArt.Utility
{
	public class VisionApi
	{
		private const string subscriptionKey = "4a3e19b7008d4a2da223c466377c2764";
		private const string endPoint = "https://westus.api.cognitive.microsoft.com/vision/v1.0/analyze";

		public async Task<VisionApiModel> MakeAnalysisRequest()
		{
			string imageFilePath = "https://moderatorsampleimages.blob.core.windows.net/samples/sample16.png";
			var errors = new List<string>();
			VisionApiModel responeData = new VisionApiModel();
			try
			{
				HttpClient client = new HttpClient();

				// Request headers.
				client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

				// Request parameters. A third optional parameter is "details".
				string requestParameters = "visualFeatures=Categories,Description,Color";

				// Assemble the URI for the REST API Call.
				string uri = endPoint + "?" + requestParameters;
				HttpResponseMessage response;

				// Request body. Posts a locally stored JPEG image.
				byte[] byteData = GetImageAsByteArray(imageFilePath);
				using (ByteArrayContent content = new ByteArrayContent(byteData))
				{
					// This example uses content type "application/octet-stream". The other content
					// types you can use are "application/json" and "multipart/form-data".
					content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

					// Make the REST API call.
					response = await client.PostAsync(uri, content);
				}

				// Get the JSON response.
				var result = await response.Content.ReadAsStringAsync();
				if (response.IsSuccessStatusCode)
				{
					responeData = JsonConvert.DeserializeObject<VisionApiModel>(result, new JsonSerializerSettings
					{
						NullValueHandling = NullValueHandling.Include,
						Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs earg)
						{
							errors.Add(earg.ErrorContext.Member.ToString());
							earg.ErrorContext.Handled = true;
						}
					});
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("\n" + e.Message);
			}
			return responeData;
		}

		private static byte[] GetImageAsByteArray(string imageFilePath)
		{
			using (FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read))
			{
				BinaryReader binaryReader = new BinaryReader(fileStream);
				return binaryReader.ReadBytes((int)fileStream.Length);
			}
		}
	}
}