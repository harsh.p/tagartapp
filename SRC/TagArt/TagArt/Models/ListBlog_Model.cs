﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using TagArt_Business.Repository;
using TagArt_Business.Model;

namespace TagArt.Models
{
    public class ListBlog_Model
    {
        public List<BlogDetailModel> BlogD_Model { get; set; }
        public BlogModel Blog_Model { get; set; }
    }
}