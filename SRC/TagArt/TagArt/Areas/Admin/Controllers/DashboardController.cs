﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using TagArt_Business.Repository;
using TagArt_Business.Model;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Drawing.Imaging;
using Elmah;

namespace TagArt.Areas.Admin.Controllers
{
	public class DashboardController : Controller
	{
		// GET: Admin/Dashboard/Index
		public ActionResult Index()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				return View();
			}
		}

		[HttpGet]
		public ActionResult GetArtistList()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWork_repository awobj = new ArtWork_repository();
					List<arts_List_Model> artsList = new List<arts_List_Model>();
					List<UserListModel> userModel = new List<UserListModel>();
					artsList = awobj.GetAllArtistName();
					UserList_repository ulr = new UserList_repository();
					List<UserListModel_Admin> ActiveUsr = ulr.GetUserList(Session["usr_UserId"].ToString()).Where(x => x.usr_Status == true).ToList();

					return Json(new { status = true, data = artsList, ActiveUsr = ActiveUsr }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = false });
					throw ex;
				}
			}
		}

		[HttpGet]
		public ActionResult GetLocationList()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWork_repository awobj = new ArtWork_repository();

					List<Location_Model> LocationList = awobj.GetLocationList().ToList();

					return Json(new { status = true, LocationList = LocationList }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = false });
					throw ex;
				}
			}
		}

		[HttpPost]
		public ActionResult DeleteArt(int ArtId)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWork_repository awobj = new ArtWork_repository();
					bool result = awobj.DeleteArtWork(ArtId);
					return Json(new { status = result }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = false });
				}
			}
		}

		public ActionResult GetArtWork(int Id, int User_Id)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWorkModel awmodel = new ArtWorkModel();
					ArtWork_repository awobj = new ArtWork_repository();
					User_Id = Convert.ToInt32(Session["usr_UserId"]);
					awmodel = awobj.GetArtWork(Id, User_Id, "0", "0");
					return Json(new { status = "success", data = awmodel }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = "fail" });
				}
			}
		}

		public ActionResult GetArtList(Searchdata data1)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					List<ArtWorkModel> awmodel = new List<ArtWorkModel>();
					ArtWork_repository awobj = new ArtWork_repository();
					awmodel = awobj.AdminDashboardArtWork(data1.Index, data1.artist, data1.Age, data1.Status, data1.StartDate, data1.EndDate, data1.UserList);
					return Json(new { status = "success", data = awmodel }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = "fail" });
				}
			}
		}

		public ActionResult DashboardFilter(Searchdata data1)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					List<ArtWorkModel> awmodel = new List<ArtWorkModel>();
					ArtWork_repository awobj = new ArtWork_repository();
					awmodel = awobj.AdminDashboardFilter(data1.Index, data1.artist, data1.Age, data1.Status);
					return Json(new { status = "success", data = awmodel }, JsonRequestBehavior.AllowGet);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = "fail" });
				}
			}
		}

		public async Task<ActionResult> InsertArts(HttpPostedFileBase file)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWorkModel artWorkModel = new ArtWorkModel();
					ArtWork_repository awobj = new ArtWork_repository();

					artWorkModel.artw_arts_Name = System.Web.HttpContext.Current.Request.Form["artist_name"];
					artWorkModel.artw_address = System.Web.HttpContext.Current.Request.Form["artist_address"];
					artWorkModel.artw_Info = System.Web.HttpContext.Current.Request.Form["artist_info"];
					artWorkModel.artw_createdby = Convert.ToInt32(Session["usr_UserId"]);
					artWorkModel.atrw_age = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["drop_ArtAge"]);
					artWorkModel.artw_latitude = System.Web.HttpContext.Current.Request.Form["latitude"];
					artWorkModel.artw_longitude = System.Web.HttpContext.Current.Request.Form["longitude"];

					string APIURL = System.Configuration.ConfigurationManager.AppSettings["APIURL"] + "api/ArtWork/AdminArtWorkInsertUpdate";
					Uri ArtInserUrl = new Uri(APIURL);
					HttpFileCollectionBase files = Request.Files;

					for (int i = 0; i < files.Count; i++)
					{
						string fname = "";
						string F_Fullname = "";

						if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
						{
							string[] testfiles = files[i].FileName.Split(new char[] { '\\' });
							fname = testfiles[testfiles.Length - 1];
						}
						string FileExt = Path.GetExtension(files[i].FileName);
						F_Fullname = artWorkModel.artw_createdby + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + FileExt;
						fname = Path.Combine(Server.MapPath("~/ImageStorage/ArtWork/"), F_Fullname);

						files[i].SaveAs(fname);
						byte[] imageArray = System.IO.File.ReadAllBytes(fname);
						string base64ImageRepresentation = Convert.ToBase64String(imageArray);

						artWorkModel.artw_image = base64ImageRepresentation;

						string output = JsonConvert.SerializeObject(artWorkModel);

						Task.Run(async () =>
						{
							using (var cli = new WebClient())
							{
								cli.Headers[HttpRequestHeader.ContentType] = "application/json";

								string response = await cli.UploadStringTaskAsync(ArtInserUrl, output);
								var obj = JsonConvert.DeserializeObject<JObject>(response);
								string status = obj["HttpStatusCode"].ToString();
							}
						}).Wait();
					}
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = "fail" });
				}
				return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
			}
		}

		private static double? GetLatitudeAndLongitude(PropertyItem propItem)
		{
			try
			{
				uint degreesNumerator = BitConverter.ToUInt32(propItem.Value, 0);
				uint degreesDenominator = BitConverter.ToUInt32(propItem.Value, 4);
				uint minutesNumerator = BitConverter.ToUInt32(propItem.Value, 8);
				uint minutesDenominator = BitConverter.ToUInt32(propItem.Value, 12);
				uint secondsNumerator = BitConverter.ToUInt32(propItem.Value, 16);
				uint secondsDenominator = BitConverter.ToUInt32(propItem.Value, 20);
				return (Convert.ToDouble(degreesNumerator) / Convert.ToDouble(degreesDenominator)) + (Convert.ToDouble(Convert.ToDouble(minutesNumerator) / Convert.ToDouble(minutesDenominator)) / 60) +
					   (Convert.ToDouble((Convert.ToDouble(secondsNumerator) / Convert.ToDouble(secondsDenominator)) / 3600));
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return null;
			}
		} // GetLatitudeAndLongitude

		public async Task<ActionResult> UpdateArts(HttpPostedFileBase file)
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					ArtWorkModel artWorkModel = new ArtWorkModel();
					ArtWork_repository awobj = new ArtWork_repository();
					artWorkModel.artw_id = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["artWork_id"]);
					artWorkModel.artw_Name = System.Web.HttpContext.Current.Request.Form["artWork_Name"];
					artWorkModel.artw_arts_Name = System.Web.HttpContext.Current.Request.Form["artist_Name"];
					artWorkModel.artw_Info = System.Web.HttpContext.Current.Request.Form["artWork_Info"];
					artWorkModel.artw_size = System.Web.HttpContext.Current.Request.Form["artWork_Size"];
					artWorkModel.atrw_age = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["artAge"]);
					artWorkModel.artw_address = System.Web.HttpContext.Current.Request.Form["artWork_Address"];
					artWorkModel.artw_latitude = System.Web.HttpContext.Current.Request.Form["artWork_latitude"];
					artWorkModel.artw_longitude = System.Web.HttpContext.Current.Request.Form["artWork_longitude"];
					artWorkModel.artw_status = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["artWork_status"]);
					artWorkModel.artw_statusNotes = System.Web.HttpContext.Current.Request.Form["artWork_statusNotes"];
					artWorkModel.artw_createdby = Convert.ToInt32(Session["usr_UserId"]);
					string ImageName = System.Web.HttpContext.Current.Request.Form["imgName"].ToString();
					string APIURL = System.Configuration.ConfigurationManager.AppSettings["APIURL"] + "api/ArtWork/ArtWorkInsertUpdate";
					Uri ArtInserUrl = new Uri(APIURL);
					HttpFileCollectionBase files = Request.Files;

					if (Request.Files.Count > 0)
					{
						for (int i = 0; i < files.Count; i++)
						{
							string fname = "";
							string F_Fullname = "";
							if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
							{
								string[] testfiles = files[i].FileName.Split(new char[] { '\\' });
								fname = testfiles[testfiles.Length - 1];
							}
							string FileExt = Path.GetExtension(files[i].FileName);
							F_Fullname = artWorkModel.artw_createdby + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + FileExt;
							fname = Path.Combine(Server.MapPath("~/ImageStorage/ArtWork/"), F_Fullname);

							files[i].SaveAs(fname);
							byte[] imageArray = System.IO.File.ReadAllBytes(fname);
							string base64ImageRepresentation = Convert.ToBase64String(imageArray);

							artWorkModel.artw_image = base64ImageRepresentation;

							string output = JsonConvert.SerializeObject(artWorkModel);

							Task.Run(async () =>
							{
								using (var cli = new WebClient())
								{
									cli.Headers[HttpRequestHeader.ContentType] = "application/json";

									string response = await cli.UploadStringTaskAsync(ArtInserUrl, output);
									var obj = JsonConvert.DeserializeObject<JObject>(response);
									string status = obj["HttpStatusCode"].ToString();
								}
							}).Wait();

							//awobj.Insert_Update_ArtWorkProfile(artWorkModel);
						}
					}
					else
					{
						artWorkModel.artw_image = ImageName;
						ArtWork_repository awpr = new ArtWork_repository();
						bool result = awpr.Insert_Update_ArtWorkProfile(artWorkModel);
						if (result == true)
						{
							return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
						}
						else
						{
							return Json(new { status = "fail" });
						}
					}

					//var responce = awobj.Insert_Update_ArtWorkProfile(artWorkModel);
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return Json(new { status = "fail" });
				}
				return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public ActionResult ArtActionChange(int ArtID, int Action)
		{
			try
			{
				ArtWork_repository awobj = new ArtWork_repository();

				bool result = awobj.Update_ArtWorkAction(ArtID, Action);

				return Json(new { status = result }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				return Json(new { status = "fail" });
			}
		}

		public ActionResult User()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					UserListModel_Admin UserModel = new UserListModel_Admin();
					UserList_repository UserObj = new UserList_repository();
					UserModel.UserList = UserObj.GetUserList(Session["usr_UserId"].ToString());
					ViewData["UserList"] = UserModel.UserList;
					return View();
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					throw ex;

					//return null;
				}
			}
		}

		public ActionResult Uploads()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					User_Art_Upload UserModel = new User_Art_Upload();
					UserDetailsListModel_Admin UserDetailsModel = new UserDetailsListModel_Admin();

					UserList_repository UserObj = new UserList_repository();
					UserModel.User_Art_UploadList = UserObj.GetUserArtUpload(Session["usr_UserId"].ToString());
					UserObj = new UserList_repository();
					UserDetailsModel.UserDetailsList = UserObj.GetUserDetailsList(Session["usr_UserId"].ToString());
					ViewData["User_Art_UploadList"] = UserModel.User_Art_UploadList;
					ViewData["UserDetailsList"] = UserDetailsModel.UserDetailsList;
					return View();
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					throw ex;

					//return null;
				}
			}
		}

		public ActionResult Comments()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					return View();
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					throw ex;

					//return null;
				}
			}
		}

		public ActionResult GetUserById(int UserId)
		{
			try
			{
				UserListModel_Admin UserModel = new UserListModel_Admin();
				UserList_repository UserObj = new UserList_repository();
				UserModel.UserList = UserObj.GetUserList(Session["usr_UserId"].ToString());
				var user = UserModel.UserList.Where(u => u.usr_UserId == UserId);
				return Json(new { user }, JsonRequestBehavior.AllowGet);

				// return Json(new { status = "success", rtype = rtype }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				throw ex;

				//return null;
			}
		}

		public ActionResult AddUser()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				try
				{
					return View();
				}
				catch (Exception ex)
				{
					var signal = ErrorSignal.FromCurrentContext();
					signal.Raise(ex);
					return null;
				}
			}
		}

		[HttpPost]
		public ActionResult SaveUser()
		{
			try
			{
				if (Session["usr_UserId"] == null)
				{
					return RedirectToAction("Login", "Login", new { area = "" });
				}
				else
				{
					try
					{
						UserListModel Usermodel = new UserListModel();
						UserList_repository UserObj = new UserList_repository();

						Usermodel.usr_FullName = (System.Web.HttpContext.Current.Request.Form["FName"]).ToString();
						Usermodel.usr_UserName = (System.Web.HttpContext.Current.Request.Form["LName"]).ToString();
						Usermodel.usr_UserEmail = (System.Web.HttpContext.Current.Request.Form["Email"]).ToString();
						Usermodel.usr_UserPassword = (System.Web.HttpContext.Current.Request.Form["Password"]).ToString();
						Usermodel.usr_Status = 1; //Convert.ToBoolean(System.Web.HttpContext.Current.Request.Form["Userstatus"]);
						Usermodel.Country = (System.Web.HttpContext.Current.Request.Form["Country"]).ToString();
						Usermodel.usr_Role = Convert.ToInt32((System.Web.HttpContext.Current.Request.Form["usr_Role"]).ToString());
                        int imgUpdate = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["imgUpdate"]);
                        string imageName = System.Web.HttpContext.Current.Request.Form["imageName"];

                        Usermodel.usr_LoginType = 3;

                        if (Request.Files.Count > 0)
                        {
                            var file = Request.Files[0];
                            string fname;
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            string FileExt = Path.GetExtension(file.FileName);
                            string F_Fullname = Usermodel.usr_UserId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + FileExt;
                            Usermodel.usr_Token = F_Fullname;
                            imageName = F_Fullname;
                            //fname = Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/"), F_Fullname);
                            fname = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["USERPROFILEIMAGEPATH"], F_Fullname);
                            //string APIURL = System.Configuration.ConfigurationManager.AppSettings["USERPROFILEIMAGEPATH"];
                            //if (!System.IO.Directory.Exists(Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/")))
                            //{
                            //    System.IO.Directory.CreateDirectory(Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/"))); //Create directory if it doesn't exist
                            //}
                            file.SaveAs(fname);
                            byte[] imageArray = System.IO.File.ReadAllBytes(fname);

                            System.IO.File.WriteAllBytes(fname, imageArray);
                            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                        }
                        Usermodel.usr_Image = imageName;

                        // Usermodel.usr_Role = 2;
                        UserList_repository ur = new UserList_repository();
						UserListModel um = new UserListModel();
						DataSet ds = new DataSet();
						ds = ur.InserUpdateUserFromAdmin(Usermodel);

						string rtype = "";
						if (ds.Tables[0].Rows.Count > 0)
						{
							rtype = ds.Tables[0].Rows[0]["rtype"].ToString();
						}

						return Json(new { status = "success", rtype = rtype }, JsonRequestBehavior.AllowGet);
					}
					catch (Exception ex)
					{
						var signal = ErrorSignal.FromCurrentContext();
						signal.Raise(ex);
						return Json(new { status = "fail" });
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		[HttpPost]
		public ActionResult UpdateUser(UserListModel Usermodel)
		{
			try
			{
				if (Session["usr_UserId"] == null)
				{
					return RedirectToAction("Login", "Login", new { area = "" });
				}
				else
				{
					try
					{
						UserList_repository UserObj = new UserList_repository();

						bool status = UserObj.UpdateUser(Usermodel);
						if (status == false)
						{
							return Json(new { status = "fail" }, JsonRequestBehavior.AllowGet);
						}

						return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
					}
					catch (Exception ex)
					{
						var signal = ErrorSignal.FromCurrentContext();
						signal.Raise(ex);
						return Json(new { status = "fail" });
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		[HttpPost]
		public ActionResult UpdateUserStatus(UserListModel Usermodel)
		{
			try
			{
				if (Session["usr_UserId"] == null)
				{
					return RedirectToAction("Login", "Login", new { area = "" });
				}
				else
				{
					try
					{
						UserList_repository UserObj = new UserList_repository();

						bool status = UserObj.UpdateUserStaus(Usermodel);
						if (status == false)
						{
							return Json(new { status = "fail" }, JsonRequestBehavior.AllowGet);
						}

						return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
					}
					catch (Exception ex)
					{
						var signal = ErrorSignal.FromCurrentContext();
						signal.Raise(ex);
						return Json(new { status = "fail" });
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		[HttpPost]
		public ActionResult DeleteUser(UserListModel Usermodel)
		{
			try
			{
				if (Session["usr_UserId"] == null)
				{
					return RedirectToAction("Login", "Login", new { area = "" });
				}
				else
				{
					try
					{
						UserList_repository UserObj = new UserList_repository();

						bool status = UserObj.DeleteUser(Usermodel);
						if (status == false)
						{
							return Json(new { status = "fail" }, JsonRequestBehavior.AllowGet);
						}

						return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
					}
					catch (Exception ex)
					{
						var signal = ErrorSignal.FromCurrentContext();
						signal.Raise(ex);
						return Json(new { status = "fail" });
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public ActionResult EditArtWorkbyID()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				return View();
			}
		}

		public ActionResult UserProfileData(int UserId)
		{
			try
			{
				UserList_repository ulr = new UserList_repository();
				UserListModel ulm = new UserListModel();

				List<ArtWorkModel> Allartlist = new List<ArtWorkModel>();
				List<ArtWorkModel> artlistBookMark = new List<ArtWorkModel>();
				List<ArtWorkModel> artlistVisited = new List<ArtWorkModel>();

				UserListUserWiseModel model = new UserListUserWiseModel();
				model.UserListModel = ulr.GetUserProfileDetail(UserId, 1);//user detail
				model.AllUsrArtWork = ulr.GetAllArtWorkByUserId("0", "0", UserId, 2); // all user art
				model.AllUsrArtWorkBookMark = ulr.GetAllArtWorkByUserId("0", "0", UserId, 4); //  only bookmark art
				model.AllUsrArtWorkVisited = ulr.GetAllArtWorkByUserId("0", "0", UserId, 3); //only visted art

				return Json(new { model }, JsonRequestBehavior.AllowGet);

				// return Json(new { status = "success", rtype = rtype }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				throw ex;

				//return null;
			}
		}

		public ActionResult UserProfileDetail()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				return View();
			}
		}

		public ActionResult UserProfile()
		{
			if (Session["usr_UserId"] == null)
			{
				return RedirectToAction("Login", "Login", new { area = "" });
			}
			else
			{
				int Usr = Convert.ToInt32(Session["usr_UserId"]);
				UserList_repository ulr = new UserList_repository();
				UserListModel ulm = new UserListModel();
				ulm = ulr.GetUserProfileDetail(Usr, 1);//user detail

				return View(ulm);
			}
		}

		[HttpPost]
		public ActionResult UserProfileData(HttpPostedFileBase file)
		{

            try
            {
				UserListModel usr = new UserListModel();
				UserList_repository usrobj = new UserList_repository();
				usr.usr_UserId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["usr_UserId"]);
				usr.usr_FullName = System.Web.HttpContext.Current.Request.Form["usr_FullName"];
				usr.usr_UserName = System.Web.HttpContext.Current.Request.Form["usr_UserName"];
				usr.usr_UserEmail = System.Web.HttpContext.Current.Request.Form["usr_UserEmail"];
				usr.usr_UserPassword = System.Web.HttpContext.Current.Request.Form["usr_UserPassword"];
				int imgUpdate = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["imgUpdate"]);
				string imageName = System.Web.HttpContext.Current.Request.Form["imageName"];
				HttpFileCollectionBase files = Request.Files;
				Boolean blnStatus = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Form["usr_Status"]);
				usr.usr_Status = Convert.ToInt32(blnStatus);
				usr.Country = System.Web.HttpContext.Current.Request.Form["Country"];

				usr.usr_Role = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["usr_Role"]);
				//usr.usr_Role = 3;
				Boolean btnusr_ass = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Form["usr_Ass"]);
				usr.usr_Ass = Convert.ToInt32(btnusr_ass);
				Boolean btnusr_upload = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Form["usr_upload"]);
				usr.usr_upload = Convert.ToInt32(btnusr_upload);

				// usr.usr_Status = true;

				string status = "";

				if (Request.Files.Count > 0)
				{
					file = files[0];
					string fname;
					if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
					{
						string[] testfiles = file.FileName.Split(new char[] { '\\' });
						fname = testfiles[testfiles.Length - 1];
					}
					string FileExt = Path.GetExtension(file.FileName);
					string F_Fullname = usr.usr_UserId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + FileExt;
					usr.usr_Token = F_Fullname;
					imageName = F_Fullname;
					//fname = Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/"), F_Fullname);
					fname = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["USERPROFILEIMAGEPATH"], F_Fullname);
                    //string APIURL = System.Configuration.ConfigurationManager.AppSettings["USERPROFILEIMAGEPATH"];
                    //if (!System.IO.Directory.Exists(Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/")))
                    //{
                    //    System.IO.Directory.CreateDirectory(Path.Combine(Server.MapPath("~/ImageStorage/UserProfile/"))); //Create directory if it doesn't exist
                    //}
                    file.SaveAs(fname);
					byte[] imageArray = System.IO.File.ReadAllBytes(fname);

                    System.IO.File.WriteAllBytes(fname, imageArray);
                    string base64ImageRepresentation = Convert.ToBase64String(imageArray);
				}
				usr.usr_Image = imageName;

				bool status1 = usrobj.UpdateUser(usr);
				if (status1 == true)
				{
					if (Convert.ToInt32(Session["usr_UserId"]) == usr.usr_UserId)
					{
						Session["usr_Image"] = imageName;
						Session["usr_UserName"] = usr.usr_UserName;
					}
				}

				return Json(new { status = status1 });
			}
			catch (Exception ex)
			{
                var signal = ErrorSignal.FromCurrentContext();
				signal.Raise(ex);
				throw ex;
			}
		}
	}

	public class Searchdata
	{
		public int Index { get; set; }

		public string artist { get; set; }

		public string Age { get; set; }

		public string Status { get; set; }

		public string UserList { get; set; }

		public string StartDate { get; set; }

		public string EndDate { get; set; }
	}
}