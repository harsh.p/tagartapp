﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using TagArt_Business.Repository;
using TagArt_Business.Model;
using System.Data;
using Elmah;

namespace TagArt.Areas.Admin.Controllers
{
    public class ArtistController : Controller
    {
        // GET: Admin/Artist
        public ActionResult Index()
        {

            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public JsonResult ArtistProfile(string Name, int index)
        {

                try
                {
                    string imagepath = System.Configuration.ConfigurationManager.AppSettings["imagesPath"];
                    List<ArtWorkModel> artWork = new List<ArtWorkModel>();
                    ArtWork_repository artWork_Repository = new ArtWork_repository();
                    artWork = artWork_Repository.GetArtWorkByArtist(Name, index, Convert.ToInt32(Session["usr_UserId"]), "0", "0");
                    return Json(new { status = "success", data = artWork }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                var signal = ErrorSignal.FromCurrentContext();
                signal.Raise(ex);
                throw ex;
                }
        }


        public JsonResult GetArtComment(int ArtID, int UserId)
        {

            UserId = Convert.ToInt32(Session["usr_UserId"]);

            ArtComment_repository artobj = new ArtComment_repository();
            try
            {

                DataSet ds = new DataSet();
                List<ArtCommentViewModel> model_List = new List<ArtCommentViewModel>();
                ArtCommentViewModel model1 = new ArtCommentViewModel();
                model1.ArtcommentReply = new List<ArtCommentReplyViewModel>();
                
                ds = artobj.GetAllArtCommentList(ArtID, UserId, 0, 1);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArtCommentViewModel model = new ArtCommentViewModel();
                    model.arc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]);
                    model.arc_artwId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_artwId"]);
                    model.arc_Desc = ds.Tables[0].Rows[i]["arc_Desc"].ToString();
                    model.arc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_UserId"]);
                    model.arc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_masterId"]);
                    model.arc_status = Convert.ToBoolean(ds.Tables[0].Rows[i]["arc_status"]);
                    model.arc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["arc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.ArtcommentReply = new List<ArtCommentReplyViewModel>();
                    List<ArtCommentReplyViewModel> childlist = new List<ArtCommentReplyViewModel>();

                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]) == (Convert.ToInt32(ds.Tables[1].Rows[j]["arc_masterId"])))
                        {
                            ArtCommentReplyViewModel ac = new ArtCommentReplyViewModel();
                            ac.arc_Id = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_Id"]);
                            ac.arc_artwId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_artwId"]);
                            ac.arc_Desc = ds.Tables[1].Rows[j]["arc_Desc"].ToString();
                            ac.arc_UserId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_UserId"]);
                            ac.arc_masterId = Convert.ToInt32(ds.Tables[1].Rows[j]["arc_masterId"]);
                            ac.arc_status = Convert.ToBoolean(ds.Tables[1].Rows[j]["arc_status"]);

                            ac.arc_CreatedDate = Convert.ToDateTime(ds.Tables[1].Rows[j]["arc_CreatedDate"]);
                            ac.usr_FullName = ds.Tables[1].Rows[j]["usr_FullName"].ToString();
                            ac.usr_Image = ds.Tables[1].Rows[j]["usr_Image"].ToString();
                            ac.LikeComment = ds.Tables[1].Rows[j]["LikeComment"].ToString();
                            ac.more = ds.Tables[1].Rows[j]["more"].ToString();

                            //  bb.bc_ModifyDate = Convert.ToDateTime(ds.Tables[0].Rows[j]["bc_ModifyDate"]);
                            childlist.Add(ac);

                        }

                    }
                    model.ArtcommentReply = childlist;


                    model_List.Add(model);

                }

         
               
                return Json(new { status = "success", data = model_List }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var signal = ErrorSignal.FromCurrentContext();
                signal.Raise(ex);
                throw ex;
            }
        }


        public JsonResult GetArtsubComment(int ArtID, int ArtCommtID, int UserId)
        {

            UserId = Convert.ToInt32(Session["usr_UserId"]);

            ArtComment_repository artobj = new ArtComment_repository();
            try
            {

                DataSet ds = new DataSet();
                List<ArtCommentViewModel> model_List = new List<ArtCommentViewModel>();
                ArtCommentViewModel model1 = new ArtCommentViewModel();
                model1.ArtcommentReply = new List<ArtCommentReplyViewModel>();

                ds = artobj.GetAllArtCommentList(ArtID, UserId, ArtCommtID, 2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArtCommentViewModel model = new ArtCommentViewModel();
                    model.arc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_Id"]);
                    model.arc_artwId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_artwId"]);
                    model.arc_Desc = ds.Tables[0].Rows[i]["arc_Desc"].ToString().Trim();
                    model.arc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_UserId"]);
                    model.arc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["arc_masterId"]);
                    model.arc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["arc_CreatedDate"]);
                    model.arc_status = Convert.ToBoolean(ds.Tables[0].Rows[i]["arc_status"]);

                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.more = ds.Tables[0].Rows[i]["more"].ToString();

                    // model.bc_ModifyDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["bc_ModifyDate"]);


                    model_List.Add(model);

                }


                return Json(new { status = "success", data = model_List }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var signal = ErrorSignal.FromCurrentContext();
                signal.Raise(ex);
                throw ex;
            }
        }


        public int AddUpdateArtComment(ArtCommentModel model)
        {

            model.arc_UserId = Convert.ToInt32(Session["usr_UserId"]);
            ArtComment_repository artcommobj = new ArtComment_repository();

            int result = artcommobj.Insert_Delete_ArtComment(model);

            return result;
        }

        public int AddDeleteArtLike(ArtCommentLikeModel model)
        {

            model.arcl_UserId = Convert.ToInt32(Session["usr_UserId"]);
            ArtCommentLike_repository ArtcommLike = new ArtCommentLike_repository();

            int result = ArtcommLike.Insert_Delete_ArtCommentLike(model);

          

                if (model.arcl_Id != 0)
                {
                    result = 0;
                }
           

                    return result;
        }

        [Route("Admin/Artist/ArtistProfilePage/{Name}")]
        public ActionResult ArtistProfilePage(string Name)
        {

            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                try
                {
                    ViewBag.artistName = Name;
                    return View();
                }
                catch (Exception ex)
                {
                    var signal = ErrorSignal.FromCurrentContext();
                    signal.Raise(ex);
                    throw ex;
                }
            }


        }
        public ActionResult GetArtListbyArtist(int Index, int Id)
        {

            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                try
                {
                    List<ArtWorkModel> awmodel = new List<ArtWorkModel>();
                    ArtWork_repository awobj = new ArtWork_repository();
                    awmodel = awobj.AdminGetArtWorkByArtist(Id, Index);
                    return Json(new { status = "success", data = awmodel }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    var signal = ErrorSignal.FromCurrentContext();
                    signal.Raise(ex);
                    return Json(new { status = "fail" });
                }

            }
        }
    }
}