﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TagArt_Business.Repository;
using TagArt_Business.Model;
using System.IO;
using TagArt.Utility;
using System.Threading.Tasks;
using System.Data;

namespace TagArt.Areas.Admin.Controllers
{
    public class BlogController : Controller
    {
        // GET: Admin/Blog
        public ActionResult Index()
        {
            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {

                return View();
            }
        }
        public ActionResult GetAllBlog(int Index, string Date_To, string Date_From, int Status)
        {
            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {

                try
                {
                    int UserId = Convert.ToInt32(Session["usr_UserId"]);
                    List<BlogModel> Blogmodel = new List<BlogModel>();
                    Blog_repository blogobj = new Blog_repository();
                    Blogmodel = blogobj.GetAllBlogWeb(Index, Date_To, Date_From, Status, UserId);


                    return Json(new { status = "success", data = Blogmodel }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {

                    return Json(new { status = "fail" });
                }
            }
        }



        public ActionResult BlogCreate()
        {
            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                return View();
            }
        }

        public void uploadnow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/ImageStorage/Blog"), ImageName);
                upload.SaveAs(path);

            }
        }
        public ActionResult uploadPartial()
        {
            var appData = Server.MapPath("~/ImageStorage/Blog");
            var images = Directory.GetFiles(appData).Select(x => new BlogimageURL
            {
                Url = Url.Content("/ImageStorage/Blog/" + Path.GetFileName(x))
            });
            return View(images);
        }

        //[HttpPost]
        //public ActionResult BlogD_RemoveItem(int bdId)
        //{
        //    if (Session["usr_UserId"] == null)
        //    {
        //        return RedirectToAction("Login", "Login", new { area = "" });
        //    }
        //    else
        //    {
        //        try
        //        {

        //            BlogDetail_repository bdr = new BlogDetail_repository();
        //            bool result = bdr.DeleteBlogDetailt(bdId);
        //            return Json(new { status = result }, JsonRequestBehavior.AllowGet);
        //        }
        //        catch (Exception ex)
        //        {

        //            return Json(new { status = "fail" });
        //        }
        //    }

        //}

        [HttpPost]
        public ActionResult Blog_Delete(int BlogId)
        {
            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                try
                {

                    Blog_repository bdr = new Blog_repository();
                    BlogModel objcd = new BlogModel();
                    objcd.bId = BlogId;
                    bool result = bdr.Delete_Blog(objcd);
                    return Json(new { status = result }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {

                    return Json(new { status = "fail" });
                }
            }

        }
        public ActionResult BlogEdit(int BID)
        {
            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                int UserID = Convert.ToInt32(Session["usr_UserId"]);
                BlogModel model = new BlogModel();
                Blog_repository obj = new Blog_repository();
                model = obj.GetBlogByID(BID, UserID);


                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]

        public async Task<ActionResult> BlogTitlesave(HttpPostedFileBase file)
        {

            if (Session["usr_UserId"] == null)
            {
                return RedirectToAction("Login", "Login", new { area = "" });
            }
            else
            {
                try
                {


                    BlogModel Blogmodel = new BlogModel();
                    Blog_repository blogobj = new Blog_repository();

                    HttpFileCollectionBase files = Request.Files;
                    Blogmodel.bId = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["bId"]);
                    Blogmodel.bTitle = (System.Web.HttpContext.Current.Request.Form["bTitle"]).ToString();
                    Blogmodel.bStatus = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Form["bStatus"]);
                    Blogmodel.bDescription = (System.Web.HttpContext.Current.Request.Form["bDecs"]).ToString();
                    Blogmodel.bCreatedBy = Convert.ToUInt16(Session["usr_UserId"]);
                    Blogmodel.bModifyBy = Convert.ToUInt16(Session["usr_UserId"]);

                    string imagePath = System.Configuration.ConfigurationManager.AppSettings["imagesPath"];


                    string F_Fullname = "";

                    if (Request.Files.Count > 0)
                    {
                        file = files[0];
                        string fname;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        String path = Server.MapPath("~/ImageStorage/Blog/");//Path
                        String path200 = Server.MapPath("~/ImageStorage/thum_Blog_20/"); //Path
                        String path240 = Server.MapPath("~/ImageStorage/thum_Blog_24/"); ;//Path


                        string FileExt = Path.GetExtension(file.FileName);
                        F_Fullname = "B_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + FileExt;
                        fname = Path.Combine(path, F_Fullname);

                        string imgPath20 = Path.Combine(path200, F_Fullname);                                                                            // string THimgname = "thum_" + data.artw_image;
                        string imgPath24 = Path.Combine(path240, F_Fullname);                                                                            // string THimgname = "thum_" + data.artw_image;

                        file.SaveAs(fname);
                        byte[] imageArray = System.IO.File.ReadAllBytes(fname);
                        MemoryStream ms = new MemoryStream(imageArray);
                        await adminsideThumbnail.Crop(240, 200, ms, imgPath20);
                        await adminsideThumbnail.Crop(240, 240, ms, imgPath24);


                        Blogmodel.bImage = F_Fullname;

                    }
                    else
                    {
                        string Titlephoto = System.Web.HttpContext.Current.Request.Form["upload_Title"];

                        Blogmodel.bImage = Titlephoto;
                    }

                    var result = blogobj.Insert_Update_BlogTitle(Blogmodel);

                    return Json(new { status = result }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {

                    return Json(new { status = "fail" });
                    throw ex;
                }

            }
        }


        public JsonResult GetBlogComment(int blogID, int UserId)
        {

            UserId = Convert.ToInt32(Session["usr_UserId"]);

            Blogcomment_repository blogobj = new Blogcomment_repository();
            try
            {

                DataSet ds = new DataSet();
                List<BlogcommentViewModel> model_List = new List<BlogcommentViewModel>();
                BlogcommentViewModel model1 = new BlogcommentViewModel();
                model1.BlogcommentReply = new List<BlogcommentReplyViewModel>();

                ds = blogobj.GetAllBlogCommentList(blogID, UserId, 0, 1);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BlogcommentViewModel model = new BlogcommentViewModel();
                    model.bc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]);
                    model.bc_BlogId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_BlogId"]);
                    model.bc_text = ds.Tables[0].Rows[i]["bc_text"].ToString();
                    model.bc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_UserId"]);
                    model.bc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_masterId"]);
                    model.bc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["bc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.BlogcommentReply = new List<BlogcommentReplyViewModel>();
                    List<BlogcommentReplyViewModel> childlist = new List<BlogcommentReplyViewModel>();

                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]) == (Convert.ToInt32(ds.Tables[1].Rows[j]["bc_masterId"])))
                        {
                            BlogcommentReplyViewModel ac = new BlogcommentReplyViewModel();
                            ac.bc_Id = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_Id"]);
                            ac.bc_BlogId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_BlogId"]);
                            ac.bc_text = ds.Tables[1].Rows[j]["bc_text"].ToString();
                            ac.bc_UserId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_UserId"]);
                            ac.bc_masterId = Convert.ToInt32(ds.Tables[1].Rows[j]["bc_masterId"]);
                            ac.bc_CreatedDate = Convert.ToDateTime(ds.Tables[1].Rows[j]["bc_CreatedDate"]);
                            ac.usr_FullName = ds.Tables[1].Rows[j]["usr_FullName"].ToString();
                            ac.usr_Image = ds.Tables[1].Rows[j]["usr_Image"].ToString();
                            ac.LikeComment = ds.Tables[1].Rows[j]["LikeComment"].ToString();
                            ac.more = ds.Tables[1].Rows[j]["more"].ToString();

                            //  bb.bc_ModifyDate = Convert.ToDateTime(ds.Tables[0].Rows[j]["bc_ModifyDate"]);
                            childlist.Add(ac);

                        }

                    }
                    model.BlogcommentReply = childlist;


                    model_List.Add(model);

                }



                return Json(new { status = "success", data = model_List }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetBlogsubComment(int blogID, int BlogCommtID, int UserId)
        {

            UserId = Convert.ToInt32(Session["usr_UserId"]);

            Blogcomment_repository blogobj = new Blogcomment_repository();
            try
            {

                DataSet ds = new DataSet();
                List<BlogcommentViewModel> model_List = new List<BlogcommentViewModel>();
                BlogcommentViewModel model1 = new BlogcommentViewModel();
                model1.BlogcommentReply = new List<BlogcommentReplyViewModel>();

                ds = blogobj.GetAllBlogCommentList(blogID, UserId, BlogCommtID, 2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BlogcommentViewModel model = new BlogcommentViewModel();
                    model.bc_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_Id"]);
                    model.bc_BlogId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_BlogId"]);
                    model.bc_text = ds.Tables[0].Rows[i]["bc_text"].ToString().Trim();
                    model.bc_UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_UserId"]);
                    model.bc_masterId = Convert.ToInt32(ds.Tables[0].Rows[i]["bc_masterId"]);
                    model.bc_CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["bc_CreatedDate"]);
                    model.usr_FullName = ds.Tables[0].Rows[i]["usr_FullName"].ToString();
                    model.usr_Image = ds.Tables[0].Rows[i]["usr_Image"].ToString();
                    model.LikeComment = ds.Tables[0].Rows[i]["LikeComment"].ToString();
                    model.more = ds.Tables[0].Rows[i]["more"].ToString();
                    model_List.Add(model);
                }
                return Json(new { status = "success", data = model_List }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddUpdateBlogComment(BlogcommentModel model)
        {
            model.bc_UserId = Convert.ToInt32(Session["usr_UserId"]);
            Blogcomment_repository blogobj = new Blogcomment_repository();

            int result = blogobj.Insert_Delete_BlogComment(model);

            return result;
        }

        public int BlogCommentLike(BlogcommentLikeModel model)
        {
            model.bcl_UserId = Convert.ToInt32(Session["usr_UserId"]);
            BlogcommentLike_repository blogcommLike = new BlogcommentLike_repository();

            int result = blogcommLike.Insert_Delete_BlogCommentLike(model);



            if (model.bcl_Id != 0)
            {
                result = 0;
            }


            return result;
        }

    }
}