﻿using System.Web.Mvc;

namespace TagArt.Areas.Admin.Controllers
{
	public class TagArtShopController : Controller
	{
		// GET: Admin/TagArtShop
		public ActionResult OpenMarketList()
		{
			return View();
		}

		public ActionResult OpenMarketDetailPage()
		{
			return View();
		}
	}
}